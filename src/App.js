import React, { useState } from "react";
import "./App.scss";

import Footer from "./components/Footer/Footer";

import HomeScreen from "./screens/HomeScreen/HomeScreen";
import BasicColorScreen from "./screens/BasicColorScreen/BasicColorScreen";
import NalloColorScreen from "./screens/NalloColorScreen/NalloColorScreen";
import XssentialColorScreen from "./screens/XssentialColorScreen/XssentialColorScreen";
import FairyScreen from "./screens/FairyScreen/FairyScreen";
import IonicScreen from "./screens/IonicScreen/IonicScreen";
import FaddyStylingScreen from "./screens/FaddyStylingScreen/FaddyStylingScreen";
import Navmenu from "./components/Navmenu/Navmenu";
import Burger from "./components/Burger/Burger";
import IColorChartScreen from "./screens/IColorChartScreen/IColorChartScreen";
import AndroidScreen from "./screens/IColorChartScreen/AndroidScreen";
import AppleScreen from "./screens/IColorChartScreen/AppleScreen";
import BasicColorChartScreen from "./screens/ColorChartScreen/BasicColorChartScreen";
import NalloColorChartScreen from "./screens/ColorChartScreen/NalloColorChartScreen";
import XssentialColorChartScreen from "./screens/ColorChartScreen/XssentialColorChartScreen";
import ColoringIssueScreen from "./screens/ColoringIssueScreen/ColoringIssueScreen";
import ColorSystemScreen from "./screens/ColorSystemScreen/ColorSystemScreen";
import KttDetailScreen from "./screens/KttDetailsScreen/KttDetailScreen";

const routeWeb = () => {
  switch (window.location.pathname) {
    case "/":
      return <HomeScreen />;
    case "/basic-color":
      return <BasicColorScreen />;
    case "/nallo-color":
      return <NalloColorScreen />;
    case "/xssential-color":
      return <XssentialColorScreen />;
    case "/fairy":
      return <FairyScreen />;
    case "/ionic":
      return <IonicScreen />;
    case "/faddy-styling":
      return <FaddyStylingScreen />;
    case "/icolorchart":
      return <IColorChartScreen />;
    case "/android":
      return <AndroidScreen />;
    case "/apple":
      return <AppleScreen />;
    case "/basic-color-chart":
      return <BasicColorChartScreen />;
    case "/nallo-color-chart":
      return <NalloColorChartScreen />;
    case "/xssential-color-chart":
      return <XssentialColorChartScreen />;
    case "/coloring-issue":
      return <ColoringIssueScreen />;
    case "/color-system":
      return <ColorSystemScreen />;
    case "/ktt-details":
      return <KttDetailScreen />;
    default:
      return <HomeScreen />;
  }
};

const currentRoute = window.location.pathname;

const App = () => {
  const [showMenu, setShowMenu] = useState(false);

  const updateShowMenu = () => {
    setShowMenu((prevShowMenu) => !prevShowMenu);
    console.log(showMenu);
  };

  let content = (
    <div>
      {routeWeb()}
      {/* <Footer /> */}
    </div>
  );

  if (showMenu) {
    content = <Navmenu show={showMenu} />;
  }

  let burgerColor = () => {
    if (!showMenu) {
      switch (currentRoute) {
        case "/ionic":
          return "#F39800";
        case "/basic-color":
        case "/fairy":
          return "#FFFFFF";
        case "/faddy-styling":
          return "#1F7BD9";

        default:
          return "#7f7f7f";
      }
    }
    return "#7f7f7f";
  };

  return (
    <div className="App">
      <Burger
        showMenu={showMenu}
        onClick={updateShowMenu}
        currentRoute={currentRoute}
        burgerColor={burgerColor()}
        style={{
          display:
            currentRoute === "/icolorchart" ||
            currentRoute === "/android" ||
            currentRoute === "/apple"
              ? "none"
              : "block",
        }}
      />
      {content}
    </div>
  );
};

export default App;
