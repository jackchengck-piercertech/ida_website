import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

import "./Burger.scss";
import FV from "../FV/FV";

const HamburgerMenuButton = (props) => {
  // const [menuShow, setMenuShow] = useState(false);

  // const updatemenuShow = () => {
  //   setMenuShow((prevMenuShow) => !prevMenuShow);
  // };

  let BurgerClassName = "Burger-Button";
  if (props.currentRoute === "/faddy-styling") {
    BurgerClassName = "Burger-Button Faddy-Burger";
  }
  return (
    <button
      // onClick={updatemenuShow}
      className={BurgerClassName}
      onClick={props.onClick}
      style={{ ...props.style }}
    >
      {/* <FV pT={"100%"} style={{ width: "30px" }}> */}
      <div
        className="Burger-Stroke"
        style={{
          backgroundColor: props.burgerColor,
        }}
      ></div>
      <div
        className="Burger-Stroke"
        style={{
          backgroundColor: props.burgerColor,
        }}
      ></div>
      <div
        className="Burger-Stroke"
        style={{
          backgroundColor: props.burgerColor,
        }}
      ></div>
      {/* <FontAwesomeIcon
          icon={faBars}
          style={{ width: "100%", height: "100%" }}
        /> */}
      {/* </FV> */}
    </button>
  );
};

export default HamburgerMenuButton;
