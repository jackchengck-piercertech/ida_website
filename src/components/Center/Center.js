import React from "react";
import "./Center.css";

const Center = (props) => {
  let containerStyle = "center";
  if (props.containerStyle) {
    containerStyle = containerStyle + " " + props.containerStyle;
  }
  return (
    <div className="centerParent">
      <div className={containerStyle}>{props.children}</div>
    </div>
  );
};

export default Center;
