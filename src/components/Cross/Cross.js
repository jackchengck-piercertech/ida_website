import React from "react";
import "./Cross.css";

import cross from "../../assets/img/cross.svg";

const Cross = (props) => {
  return (
    <div className="Cross-Container" onClick={props.onClick}>
      <img src={cross} alt="" />
    </div>
  );
};

export default Cross;
