import React from "react";
import "./DropletButton.css";

const DropletButton = (props) => {
  const className = "DropletButton " + props.className;
  return <button className={className}>{props.children}</button>;
};

export default DropletButton;
