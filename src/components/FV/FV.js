import React from "react";
import "./FV.css";

const FV = (props) => {
  return (
    <div
      className="FVContainer"
      style={{ ...props.style, paddingTop: props.pT }}
    >
      <div>{props.children}</div>
    </div>
  );
};

export default FV;
