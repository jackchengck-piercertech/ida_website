import React from "react";
import "./Footer.css";

const Footer = (props) => {
  let className = props.className ? "Footer " + props.className : "Footer";

  return (
    <div className={className} style={props.style}>
      Copyright © 2020 World Hair Cosmetics (Asia) Ltd. All Rights Reserved.
    </div>
  );
};

export default Footer;
