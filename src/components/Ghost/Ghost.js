import React, { useState } from "react";
import useInterval from "../useInterval";

import "./Ghost.css";

const Ghost = (props) => {
  const [timeCount, setTimeCount] = useState(0);

  let timeMax = 30;
  if (props.timeMax) {
    timeMax = props.timeMax;
  }

  const updateTimeCount = () => {
    setTimeCount((prevTimeCount) => {
      if (prevTimeCount === timeMax) {
        return 0;
      }
      return prevTimeCount + 1;
    });
  };

  useInterval(() => {
    // 你自己的代码
    updateTimeCount();
  }, props.timer);

  return (
    <div
      className={
        timeCount % props.timeC === props.active
          ? "opacity-animation"
          : "opacity-animation hide"
      }
      style={{ ...props.style }}
    >
      {props.children}
    </div>
  );
};

export default Ghost;
