import React from "react";

const Modal = (props) => {
  return (
    <div>
      <div>{props.children}</div>
    </div>
  );
};

export default Modal;
