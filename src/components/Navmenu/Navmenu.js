import React from "react";
import "./Navmenu.css";

import Footer from "../Footer/Footer";
import Center from "../Center/Center";

const Navmenu = (props) => {
  return (
    <div className="Navmenu">
      <ul>
        <li>
          <a href="/" className="One">
            Home
          </a>
        </li>
        <li>
          <a href="/basic-color" className="Two">
            Basic Color
          </a>
        </li>
        <li>
          <a href="/nallo-color" className="Three">
            Nallo Color
          </a>
        </li>
        <li>
          <a href="/xssential-color" className="Four">
            Xssential Color
          </a>
        </li>
        <li>
          <a href="/fairy" className="Five">
            Fairy
          </a>
        </li>
        <li>
          <a href="/ionic" className="Six">
            Ionic
          </a>
        </li>
        <li>
          <a href="/faddy-styling" className="Seven">
            Faddy Styling
          </a>
        </li>
      </ul>
      <Footer className="Nav" />
    </div>
  );
};

export default Navmenu;
