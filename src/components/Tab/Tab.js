import React from "react";
import "./Tab.css";

const Tab = (props) => {
  const className = props.className ? "Tab " + props.className : "Tab";
  return (
    <div className={className} onClick={props.onClick}>
      {props.children}
    </div>
  );
};

export default Tab;
