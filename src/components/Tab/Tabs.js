import React from "react";
import "./Tabs.css";

const Tabs = (props) => {
  return <div className="Tabs">{props.children}</div>;
};

export default Tabs;
