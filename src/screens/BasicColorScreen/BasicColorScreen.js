import React, { useState, useEffect } from "react";
import "./BasicColorScreen.css";
import Ghost from "../../components/Ghost/Ghost";
import { Modal } from "@material-ui/core";
import DropletButton from "../../components/DropletButton/DropletButton";
import AnchorLink from "react-anchor-link-smooth-scroll";
import { Carousel } from "react-responsive-carousel";

import banner1T from "../../assets/img/basic-color/svg-177004x59.svg";
import banner2T1 from "../../assets/img/basic-color/web page-23.png";
import banner2T2 from "../../assets/img/basic-color/svg-262x69.svg";
import p1 from "../../assets/img/basic-color/basic 4colors.png";
import p1d from "../../assets/img/basic-color/svg-534752x205.svg";
import CCButton from "../../assets/img/basic-color/svg-55336x9.svg";
import CIButton from "../../assets/img/basic-color/svg-70781x11.svg";
import CSButton from "../../assets/img/basic-color/svg-25629x43.svg";

import ChartT from "../../assets/img/basic-color/svg-562092x41.svg";
import ChartD from "../../assets/img/basic-color/svg-935012x120.svg";

import carousel1 from "../../assets/img/basic-color/basic label01.png";
import carousel2 from "../../assets/img/basic-color/basic label02.png";
import carousel3 from "../../assets/img/basic-color/basic label03.png";
import carousel4 from "../../assets/img/basic-color/basic label04.png";

import BSCT from "../../assets/img/basic-color/svg-158978x37.svg";
import BSCp1 from "../../assets/img/basic-color/basic ball.png";
import BSCp2 from "../../assets/img/basic-color/basic p ball.png";
import BSCp3 from "../../assets/img/basic-color/basic l ball.png";

import in1 from "../../assets/img/basic-color/svg-411737x195.svg";
import in2 from "../../assets/img/basic-color/svg-418861x213.svg";
import in3 from "../../assets/img/basic-color/svg-424787x259.svg";

import BTT from "../../assets/img/basic-color/svg-12876x59.svg";
import BTp from "../../assets/img/basic-color/basic toner.png";
import BTd from "../../assets/img/basic-color/svg-443473x240.svg";
import BTp1 from "../../assets/img/basic-color/p300 ball.png";
import BTp2 from "../../assets/img/basic-color/c150 basll.png";
import BTp3 from "../../assets/img/basic-color/c180 ball_2.png";
import BTp1t from "../../assets/img/basic-color/svg-141581x13.svg";
import BTp2t from "../../assets/img/basic-color/svg-143099x13.svg";
import BTp3t from "../../assets/img/basic-color/svg-21378x13.svg";

import topButton from "../../assets/img/nallo-color/svg 39x39_poster_.png";

const BasicColorScreen = (props) => {
  const [curModal, setCurModal] = useState(0);
  const [modalOpen, setModalOpen] = useState(false);
  useEffect(() => {
    document.title = "Basic Color";
  });
  const handleModalOpen = (contentNum) => {
    setCurModal(contentNum);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  let modalContent = <div className="BC-Modal-1">This is Modal</div>;

  switch (curModal) {
    case 1:
      modalContent = (
        <div className="BC-Modal-1">
          <img src={in1} alt="" />
        </div>
      );

      break;
    case 2:
      modalContent = (
        <div className="BC-Modal-2">
          <img src={in2} alt="" />
        </div>
      );

      break;
    case 3:
      modalContent = (
        <div className="BC-Modal-3">
          <img src={in3} alt="" />
        </div>
      );

      break;
    default:
      modalContent = <div className="BC-Modal-1">This is Modal</div>;
  }

  return (
    <div className="BC-Main">
      <Modal open={modalOpen} onClose={handleModalClose}>
        <div className="BC-Modal">{modalContent}</div>
      </Modal>
      <div className="BC-Banner" id="bcBanner">
        <Ghost timer={5000} timeC={2} active={0}>
          <div className="BC-Banner-1">
            <div className="BC-Banner-1-Title">
              <img src={banner1T} alt="" />
            </div>
            {/* <div className="BC-Banner-1-Pic">
              <img src={banner1P} alt="" />
            </div> */}
          </div>
        </Ghost>
        <Ghost timer={5000} timeC={2} active={1}>
          <div className="BC-Banner-2">
            <div className="BC-Banner-2-Container">
              <div className="BC-Banner-2-Logo">
                <img src={banner2T1} alt="" />
                {/* <FV pT={"100%"} style={{ background: "#2e3192" }}>
                </FV> */}
              </div>
              <div className="BC-Banner-2-Title">
                <img src={banner2T2} alt="" />
              </div>
            </div>
          </div>
        </Ghost>
      </div>
      <div className="BC-Description">
        <div className="BC-Description-Container">
          <div>
            <img src={p1} alt="" />
          </div>
          <div>
            <img src={p1d} alt="" />
            <div className="BC-Description-Button">
              <DropletButton className="blueBg">
                <a href="/basic-color-chart">
                  <img
                    src={CCButton}
                    alt=""
                    // style={{ height: "1rem", width: "auto" }}
                  />
                </a>
              </DropletButton>
              <DropletButton className="blue">
                <a href="/coloring-issue">
                  <img
                    src={CIButton}
                    alt=""
                    // style={{ height: "1rem", width: "auto" }}
                  />
                </a>
              </DropletButton>
              <DropletButton className="blue">
                <a href="/color-system">
                  <img
                    src={CSButton}
                    alt=""
                    // style={{ height: "1rem", width: "auto" }}
                  />
                </a>
              </DropletButton>
            </div>
          </div>
        </div>
      </div>
      <div className="BC-ColorChart">
        <div className="BC-ColorChart-Title">
          <img src={ChartT} alt="" />
        </div>
        <div className="BC-ColorChart-Banner"></div>
        <div className="BC-ColorChart-Description">
          <img src={ChartD} alt="" />
        </div>
        <div>
          <Carousel
            showArrows={true}
            showThumbs={false}
            showIndicators={false}
            showStatus={false}
            autoPlay={true}
            infiniteLoop={true}
            swipeable={true}
          >
            <div style={{ background: "#c8c8c8" }}>
              <img src={carousel1} alt="" />
            </div>
            <div style={{ background: "#c8c8c8" }}>
              <img src={carousel2} alt="" />
            </div>
            <div style={{ background: "#c8c8c8" }}>
              <img src={carousel3} alt="" />
            </div>
            <div style={{ background: "#c8c8c8" }}>
              <img src={carousel4} alt="" />
            </div>
          </Carousel>
        </div>
      </div>
      <div className="BC-BCS">
        <div className="BC-BCS-Title">
          <img src={BSCT} alt="" />
        </div>
        <div className="BC-BCS-Product">
          <div onClick={() => handleModalOpen(1)}>
            <img src={BSCp1} alt="" />
          </div>
          <div onClick={() => handleModalOpen(2)}>
            <img src={BSCp2} alt="" />
          </div>
          <div onClick={() => handleModalOpen(3)}>
            <img src={BSCp3} alt="" />
          </div>
        </div>
      </div>
      <div className="BC-BT">
        <div className="BC-BT-Title">
          <img src={BTT} alt="" />
        </div>
        <div className="BC-BT-Product">
          <div className="BC-BT-Container">
            <div>
              <img src={BTp} alt="" />
            </div>
            <div>
              <img src={BTd} alt="" />
            </div>
          </div>
          <div className="BC-BT-Products">
            <div className="BC-BT-Products-Box">
              <div className="BC-BT-Products-P">
                <img src={BTp1} alt="" />
              </div>
              <div className="BC-BT-Products-T">
                <img src={BTp1t} alt="" />
              </div>
            </div>
            <div className="BC-BT-Products-Box">
              <div className="BC-BT-Products-P">
                <img src={BTp2} alt="" />
              </div>
              <div className="BC-BT-Products-T">
                <img src={BTp2t} alt="" />
              </div>
            </div>
            <div className="BC-BT-Products-Box">
              <div className="BC-BT-Products-P">
                <img src={BTp3} alt="" />
              </div>
              <div className="BC-BT-Products-T">
                <img src={BTp3t} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="BC-TopButton">
        <AnchorLink href="#bcBanner">
          <img src={topButton} alt="" />
        </AnchorLink>
      </div>
    </div>
  );
};

export default BasicColorScreen;
