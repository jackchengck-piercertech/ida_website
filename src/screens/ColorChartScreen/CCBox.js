import React from "react";
import "./CCBox.css";
import FV from "../../components/FV/FV";

const CCBox = (props) => {
  let imgContent;
  let hovImgContent;

  // const onHoverHandler = () => {
  //   if (props.hovImg) {
  //     imgContent = <img src={props.hovImg} alt="" />;
  //     console.log("Hover");
  //   }
  // };

  if (props.srcImg) {
    imgContent = (
      <div
        className={
          props.hovImg
            ? "CC-Section-Img-Container hover"
            : "CC-Section-Img-Container"
        }
      >
        <img className="CC-Section-Img" src={props.srcImg} alt="" />
      </div>
    );
  }

  if (props.hovImg) {
    hovImgContent = (
      <div className="CC-Section-Img-Container no-hover">
        <img className="CC-Section-Img" src={props.hovImg} alt="" />
      </div>
    );
  }

  return (
    <div
      className={props.onClick ? "CC-Section-Box clickable" : "CC-Section-Box"}
      onClick={props.onClick}
    >
      <FV pT={"100%"}>
        {imgContent}
        {hovImgContent}
      </FV>
    </div>
  );
};
export default CCBox;
