import React, { useState, useEffect } from "react";
import "./ColorChartScreen.css";
import CCBox from "./CCBox";
import { Modal, Fade } from "@material-ui/core";

import T1 from "../../assets/img/color-chart/hair-color/title/1.jpg";
import T2 from "../../assets/img/color-chart/hair-color/title/2.jpg";
import T3 from "../../assets/img/color-chart/hair-color/title/3.jpg";
import T4 from "../../assets/img/color-chart/hair-color/title/4.jpg";
import T5 from "../../assets/img/color-chart/hair-color/title/5.jpg";
import T6 from "../../assets/img/color-chart/hair-color/title/6.jpg";
import T7 from "../../assets/img/color-chart/hair-color/title/7.jpg";
import T8 from "../../assets/img/color-chart/hair-color/title/8.jpg";
import T9 from "../../assets/img/color-chart/hair-color/title/9.jpg";
import T10 from "../../assets/img/color-chart/hair-color/title/10.jpg";
import T11 from "../../assets/img/color-chart/hair-color/title/11.jpg";
import T12 from "../../assets/img/color-chart/hair-color/title/12.jpg";
import T13 from "../../assets/img/color-chart/hair-color/title/13.jpg";
import T14 from "../../assets/img/color-chart/hair-color/title/14.jpg";
import T15 from "../../assets/img/color-chart/hair-color/title/15.jpg";
import T16 from "../../assets/img/color-chart/hair-color/title/16.jpg";
import T17 from "../../assets/img/color-chart/hair-color/title/17.jpg";
import T18 from "../../assets/img/color-chart/hair-color/title/18.jpg";
import T19 from "../../assets/img/color-chart/hair-color/title/19.jpg";

import n1 from "../../assets/img/color-chart/hair-color/n/1.jpg";
import n2 from "../../assets/img/color-chart/hair-color/n/2.jpg";
import n3 from "../../assets/img/color-chart/hair-color/n/3.jpg";
import n4 from "../../assets/img/color-chart/hair-color/n/4.jpg";
import n5 from "../../assets/img/color-chart/hair-color/n/5.jpg";
import n6 from "../../assets/img/color-chart/hair-color/n/6.jpg";
import n7 from "../../assets/img/color-chart/hair-color/n/7.jpg";
import n8 from "../../assets/img/color-chart/hair-color/n/8.jpg";
import n9 from "../../assets/img/color-chart/hair-color/n/9.jpg";
import n10 from "../../assets/img/color-chart/hair-color/n/10.jpg";

import a1 from "../../assets/img/color-chart/hair-color/a/1.jpg";
import a2 from "../../assets/img/color-chart/hair-color/a/2.jpg";
import a3 from "../../assets/img/color-chart/hair-color/a/3.jpg";
import a4 from "../../assets/img/color-chart/hair-color/a/4.jpg";
import a5 from "../../assets/img/color-chart/hair-color/a/5.jpg";
import a6 from "../../assets/img/color-chart/hair-color/a/6.jpg";

import g1 from "../../assets/img/color-chart/hair-color/g/1.jpg";
import g2 from "../../assets/img/color-chart/hair-color/g/2.jpg";
import g3 from "../../assets/img/color-chart/hair-color/g/3.jpg";
import g4 from "../../assets/img/color-chart/hair-color/g/4.jpg";
import g5 from "../../assets/img/color-chart/hair-color/g/5.jpg";
import g6 from "../../assets/img/color-chart/hair-color/g/6.jpg";

import gg1 from "../../assets/img/color-chart/hair-color/gg/1.jpg";
import gg2 from "../../assets/img/color-chart/hair-color/gg/2.jpg";
import gg3 from "../../assets/img/color-chart/hair-color/gg/3.jpg";
import gg4 from "../../assets/img/color-chart/hair-color/gg/4.jpg";

import gb1 from "../../assets/img/color-chart/hair-color/gb/1.jpg";
import gb2 from "../../assets/img/color-chart/hair-color/gb/2.jpg";
import gb3 from "../../assets/img/color-chart/hair-color/gb/3.jpg";

import b1 from "../../assets/img/color-chart/hair-color/b/1.jpg";
import b2 from "../../assets/img/color-chart/hair-color/b/2.jpg";
import b3 from "../../assets/img/color-chart/hair-color/b/3.jpg";
import b4 from "../../assets/img/color-chart/hair-color/b/4.jpg";

import br1 from "../../assets/img/color-chart/hair-color/br/1.jpg";
import br2 from "../../assets/img/color-chart/hair-color/br/2.jpg";
import br3 from "../../assets/img/color-chart/hair-color/br/3.jpg";
import br4 from "../../assets/img/color-chart/hair-color/br/4.jpg";

import rb1 from "../../assets/img/color-chart/hair-color/rb/1.jpg";
import rb2 from "../../assets/img/color-chart/hair-color/rb/2.jpg";
import rb3 from "../../assets/img/color-chart/hair-color/rb/3.jpg";
import rb4 from "../../assets/img/color-chart/hair-color/rb/4.jpg";

import kr1 from "../../assets/img/color-chart/hair-color/kr/1.jpg";
import kr2 from "../../assets/img/color-chart/hair-color/kr/2.jpg";

import k1 from "../../assets/img/color-chart/hair-color/k/1.jpg";
import k2 from "../../assets/img/color-chart/hair-color/k/2.jpg";
import k3 from "../../assets/img/color-chart/hair-color/k/3.jpg";
import k4 from "../../assets/img/color-chart/hair-color/k/4.jpg";

import kg1 from "../../assets/img/color-chart/hair-color/kg/1.jpg";
import kg2 from "../../assets/img/color-chart/hair-color/kg/2.jpg";
import kg3 from "../../assets/img/color-chart/hair-color/kg/3.jpg";
import kg4 from "../../assets/img/color-chart/hair-color/kg/4.jpg";
import kg5 from "../../assets/img/color-chart/hair-color/kg/5.jpg";
import kg6 from "../../assets/img/color-chart/hair-color/kg/6.jpg";
import kg7 from "../../assets/img/color-chart/hair-color/kg/7.jpg";

import r1 from "../../assets/img/color-chart/hair-color/r/1.jpg";
import r2 from "../../assets/img/color-chart/hair-color/r/2.jpg";
import r3 from "../../assets/img/color-chart/hair-color/r/3.jpg";
import r4 from "../../assets/img/color-chart/hair-color/r/4.jpg";

import v1 from "../../assets/img/color-chart/hair-color/v/1.jpg";
import v2 from "../../assets/img/color-chart/hair-color/v/2.jpg";
import v3 from "../../assets/img/color-chart/hair-color/v/3.jpg";

import vr1 from "../../assets/img/color-chart/hair-color/vr/1.jpg";
import vr2 from "../../assets/img/color-chart/hair-color/vr/2.jpg";

import ub1 from "../../assets/img/color-chart/hair-color/ultra-blond/1.jpg";
import ub2 from "../../assets/img/color-chart/hair-color/ultra-blond/2.jpg";
import ub3 from "../../assets/img/color-chart/hair-color/ultra-blond/3.jpg";
import ub4 from "../../assets/img/color-chart/hair-color/ultra-blond/4.jpg";
import ub5 from "../../assets/img/color-chart/hair-color/ultra-blond/5.jpg";
import ub6 from "../../assets/img/color-chart/hair-color/ultra-blond/6.jpg";
import ub7 from "../../assets/img/color-chart/hair-color/ultra-blond/7.jpg";

import mix1 from "../../assets/img/color-chart/hair-color/mix/1.jpg";
import mix2 from "../../assets/img/color-chart/hair-color/mix/2.jpg";
import mix3 from "../../assets/img/color-chart/hair-color/mix/3.jpg";
import mix4 from "../../assets/img/color-chart/hair-color/mix/4.jpg";
import mix5 from "../../assets/img/color-chart/hair-color/mix/5.jpg";

import h1 from "../../assets/img/color-chart/hair-color/100/1.jpg";
import h2 from "../../assets/img/color-chart/hair-color/100/2.jpg";
import h3 from "../../assets/img/color-chart/hair-color/100/3.jpg";
import h4 from "../../assets/img/color-chart/hair-color/100/4.jpg";
import h5 from "../../assets/img/color-chart/hair-color/100/5.jpg";
import h6 from "../../assets/img/color-chart/hair-color/100/6.jpg";
import h7 from "../../assets/img/color-chart/hair-color/100/7.jpg";
import h8 from "../../assets/img/color-chart/hair-color/100/8.jpg";
import h9 from "../../assets/img/color-chart/hair-color/100/9.jpg";
import h10 from "../../assets/img/color-chart/hair-color/100/10.jpg";

import bc1 from "../../assets/img/color-chart/hair-color/blonding-cream/1.jpg";
import mt1 from "../../assets/img/color-chart/hair-color/mix-tone/1.jpg";

// HOVER IMAGE
import n1h from "../../assets/img/color-chart/hair-color/n/hover/1.jpg";
import n2h from "../../assets/img/color-chart/hair-color/n/hover/2.jpg";
import n3h from "../../assets/img/color-chart/hair-color/n/hover/3.jpg";
import n4h from "../../assets/img/color-chart/hair-color/n/hover/4.jpg";
import n5h from "../../assets/img/color-chart/hair-color/n/hover/5.jpg";
import n6h from "../../assets/img/color-chart/hair-color/n/hover/6.jpg";
import n7h from "../../assets/img/color-chart/hair-color/n/hover/7.jpg";
import n8h from "../../assets/img/color-chart/hair-color/n/hover/8.jpg";
import n9h from "../../assets/img/color-chart/hair-color/n/hover/9.jpg";
import n10h from "../../assets/img/color-chart/hair-color/n/hover/10.jpg";

import a1h from "../../assets/img/color-chart/hair-color/a/hover/1.jpg";
import a2h from "../../assets/img/color-chart/hair-color/a/hover/2.jpg";
import a3h from "../../assets/img/color-chart/hair-color/a/hover/3.jpg";
import a4h from "../../assets/img/color-chart/hair-color/a/hover/4.jpg";
import a5h from "../../assets/img/color-chart/hair-color/a/hover/5.jpg";
import a6h from "../../assets/img/color-chart/hair-color/a/hover/6.jpg";

import g1h from "../../assets/img/color-chart/hair-color/g/hover/1.jpg";
import g2h from "../../assets/img/color-chart/hair-color/g/hover/2.jpg";
import g3h from "../../assets/img/color-chart/hair-color/g/hover/3.jpg";
import g4h from "../../assets/img/color-chart/hair-color/g/hover/4.jpg";
import g5h from "../../assets/img/color-chart/hair-color/g/hover/5.jpg";
import g6h from "../../assets/img/color-chart/hair-color/g/hover/6.jpg";

import gg1h from "../../assets/img/color-chart/hair-color/gg/hover/1.jpg";
import gg2h from "../../assets/img/color-chart/hair-color/gg/hover/2.jpg";
import gg3h from "../../assets/img/color-chart/hair-color/gg/hover/3.jpg";
import gg4h from "../../assets/img/color-chart/hair-color/gg/hover/4.jpg";

import gb1h from "../../assets/img/color-chart/hair-color/gb/hover/1.jpg";
import gb2h from "../../assets/img/color-chart/hair-color/gb/hover/2.jpg";
import gb3h from "../../assets/img/color-chart/hair-color/gb/hover/3.jpg";

import b1h from "../../assets/img/color-chart/hair-color/b/hover/1.jpg";
import b2h from "../../assets/img/color-chart/hair-color/b/hover/2.jpg";
import b3h from "../../assets/img/color-chart/hair-color/b/hover/3.jpg";
import b4h from "../../assets/img/color-chart/hair-color/b/hover/4.jpg";

import br1h from "../../assets/img/color-chart/hair-color/br/hover/1.jpg";
import br2h from "../../assets/img/color-chart/hair-color/br/hover/2.jpg";
import br3h from "../../assets/img/color-chart/hair-color/br/hover/3.jpg";
import br4h from "../../assets/img/color-chart/hair-color/br/hover/4.jpg";

import rb1h from "../../assets/img/color-chart/hair-color/rb/hover/1.jpg";
import rb2h from "../../assets/img/color-chart/hair-color/rb/hover/2.jpg";
import rb3h from "../../assets/img/color-chart/hair-color/rb/hover/3.jpg";
import rb4h from "../../assets/img/color-chart/hair-color/rb/hover/4.jpg";

import kr1h from "../../assets/img/color-chart/hair-color/kr/hover/1.jpg";
import kr2h from "../../assets/img/color-chart/hair-color/kr/hover/2.jpg";

import k1h from "../../assets/img/color-chart/hair-color/k/hover/1.jpg";
import k2h from "../../assets/img/color-chart/hair-color/k/hover/2.jpg";
import k3h from "../../assets/img/color-chart/hair-color/k/hover/3.jpg";
import k4h from "../../assets/img/color-chart/hair-color/k/hover/4.jpg";

import kg1h from "../../assets/img/color-chart/hair-color/kg/hover/1.jpg";
import kg2h from "../../assets/img/color-chart/hair-color/kg/hover/2.jpg";
import kg3h from "../../assets/img/color-chart/hair-color/kg/hover/3.jpg";
import kg4h from "../../assets/img/color-chart/hair-color/kg/hover/4.jpg";
import kg5h from "../../assets/img/color-chart/hair-color/kg/hover/5.jpg";
import kg6h from "../../assets/img/color-chart/hair-color/kg/hover/6.jpg";
import kg7h from "../../assets/img/color-chart/hair-color/kg/hover/7.jpg";

import r1h from "../../assets/img/color-chart/hair-color/r/hover/1.jpg";
import r2h from "../../assets/img/color-chart/hair-color/r/hover/2.jpg";
import r3h from "../../assets/img/color-chart/hair-color/r/hover/3.jpg";
import r4h from "../../assets/img/color-chart/hair-color/r/hover/4.jpg";

import v1h from "../../assets/img/color-chart/hair-color/v/hover/1.jpg";
import v2h from "../../assets/img/color-chart/hair-color/v/hover/2.jpg";
import v3h from "../../assets/img/color-chart/hair-color/v/hover/3.jpg";

import vr1h from "../../assets/img/color-chart/hair-color/vr/hover/1.jpg";
import vr2h from "../../assets/img/color-chart/hair-color/vr/hover/2.jpg";

import ub1h from "../../assets/img/color-chart/hair-color/ultra-blond/hover/1.jpg";
import ub2h from "../../assets/img/color-chart/hair-color/ultra-blond/hover/2.jpg";
import ub3h from "../../assets/img/color-chart/hair-color/ultra-blond/hover/3.jpg";
import ub4h from "../../assets/img/color-chart/hair-color/ultra-blond/hover/4.jpg";
import ub5h from "../../assets/img/color-chart/hair-color/ultra-blond/hover/5.jpg";
import ub6h from "../../assets/img/color-chart/hair-color/ultra-blond/hover/6.jpg";
import ub7h from "../../assets/img/color-chart/hair-color/ultra-blond/hover/7.jpg";

import mix1h from "../../assets/img/color-chart/hair-color/mix/hover/1.jpg";
import mix2h from "../../assets/img/color-chart/hair-color/mix/hover/2.jpg";
import mix3h from "../../assets/img/color-chart/hair-color/mix/hover/3.jpg";
import mix4h from "../../assets/img/color-chart/hair-color/mix/hover/4.jpg";
import mix5h from "../../assets/img/color-chart/hair-color/mix/hover/5.jpg";

import h1h from "../../assets/img/color-chart/hair-color/100/hover/1.jpg";
import h2h from "../../assets/img/color-chart/hair-color/100/hover/2.jpg";
import h3h from "../../assets/img/color-chart/hair-color/100/hover/3.jpg";
import h4h from "../../assets/img/color-chart/hair-color/100/hover/4.jpg";
import h5h from "../../assets/img/color-chart/hair-color/100/hover/5.jpg";
import h6h from "../../assets/img/color-chart/hair-color/100/hover/6.jpg";
import h7h from "../../assets/img/color-chart/hair-color/100/hover/7.jpg";
import h8h from "../../assets/img/color-chart/hair-color/100/hover/8.jpg";
import h9h from "../../assets/img/color-chart/hair-color/100/hover/9.jpg";
import h10h from "../../assets/img/color-chart/hair-color/100/hover/10.jpg";

import bc1h from "../../assets/img/color-chart/hair-color/blonding-cream/hover/1.jpg";
import mt1h from "../../assets/img/color-chart/hair-color/mix-tone/hover/1.jpg";

// CLICK IMAGE

import n1c from "../../assets/img/color-chart/hair-color/n/nclick/1.jpg";
import n2c from "../../assets/img/color-chart/hair-color/n/nclick/2.jpg";
import n3c from "../../assets/img/color-chart/hair-color/n/nclick/3.jpg";
import n4c from "../../assets/img/color-chart/hair-color/n/nclick/4.jpg";
import n5c from "../../assets/img/color-chart/hair-color/n/nclick/5.jpg";
import n6c from "../../assets/img/color-chart/hair-color/n/nclick/6.jpg";
import n7c from "../../assets/img/color-chart/hair-color/n/nclick/7.jpg";
import n8c from "../../assets/img/color-chart/hair-color/n/nclick/8.jpg";
import n9c from "../../assets/img/color-chart/hair-color/n/nclick/9.jpg";
import n10c from "../../assets/img/color-chart/hair-color/n/nclick/10.jpg";

import a1c from "../../assets/img/color-chart/hair-color/a/nclick/1.jpg";
import a2c from "../../assets/img/color-chart/hair-color/a/nclick/2.jpg";
import a3c from "../../assets/img/color-chart/hair-color/a/nclick/3.jpg";
import a4c from "../../assets/img/color-chart/hair-color/a/nclick/4.jpg";
import a5c from "../../assets/img/color-chart/hair-color/a/nclick/5.jpg";
import a6c from "../../assets/img/color-chart/hair-color/a/nclick/6.jpg";

import g1c from "../../assets/img/color-chart/hair-color/g/nclick/1.jpg";
import g2c from "../../assets/img/color-chart/hair-color/g/nclick/2.jpg";
import g3c from "../../assets/img/color-chart/hair-color/g/nclick/3.jpg";
import g4c from "../../assets/img/color-chart/hair-color/g/nclick/4.jpg";
import g5c from "../../assets/img/color-chart/hair-color/g/nclick/5.jpg";
import g6c from "../../assets/img/color-chart/hair-color/g/nclick/6.jpg";

import gg1c from "../../assets/img/color-chart/hair-color/gg/nclick/1.jpg";
import gg2c from "../../assets/img/color-chart/hair-color/gg/nclick/2.jpg";
import gg3c from "../../assets/img/color-chart/hair-color/gg/nclick/3.jpg";
import gg4c from "../../assets/img/color-chart/hair-color/gg/nclick/4.jpg";

import gb1c from "../../assets/img/color-chart/hair-color/gb/nclick/1.jpg";
import gb2c from "../../assets/img/color-chart/hair-color/gb/nclick/2.jpg";
import gb3c from "../../assets/img/color-chart/hair-color/gb/nclick/3.jpg";

import b1c from "../../assets/img/color-chart/hair-color/b/nclick/1.jpg";
import b2c from "../../assets/img/color-chart/hair-color/b/nclick/2.jpg";
import b3c from "../../assets/img/color-chart/hair-color/b/nclick/3.jpg";
import b4c from "../../assets/img/color-chart/hair-color/b/nclick/4.jpg";

import br1c from "../../assets/img/color-chart/hair-color/br/nclick/1.jpg";
import br2c from "../../assets/img/color-chart/hair-color/br/nclick/2.jpg";
import br3c from "../../assets/img/color-chart/hair-color/br/nclick/3.jpg";
import br4c from "../../assets/img/color-chart/hair-color/br/nclick/4.jpg";

import kr1c from "../../assets/img/color-chart/hair-color/kr/nclick/1.jpg";
import kr2c from "../../assets/img/color-chart/hair-color/kr/nclick/2.jpg";

import k1c from "../../assets/img/color-chart/hair-color/k/nclick/1.jpg";
import k2c from "../../assets/img/color-chart/hair-color/k/nclick/2.jpg";
import k3c from "../../assets/img/color-chart/hair-color/k/nclick/3.jpg";
import k4c from "../../assets/img/color-chart/hair-color/k/nclick/4.jpg";

import kg1c from "../../assets/img/color-chart/hair-color/kg/nclick/1.jpg";
import kg2c from "../../assets/img/color-chart/hair-color/kg/nclick/2.jpg";
import kg3c from "../../assets/img/color-chart/hair-color/kg/nclick/3.jpg";
import kg4c from "../../assets/img/color-chart/hair-color/kg/nclick/4.jpg";
import kg5c from "../../assets/img/color-chart/hair-color/kg/nclick/5.jpg";
import kg6c from "../../assets/img/color-chart/hair-color/kg/nclick/6.jpg";
import kg7c from "../../assets/img/color-chart/hair-color/kg/nclick/7.jpg";

import r1c from "../../assets/img/color-chart/hair-color/r/nclick/1.jpg";
import r2c from "../../assets/img/color-chart/hair-color/r/nclick/2.jpg";
import r3c from "../../assets/img/color-chart/hair-color/r/nclick/3.jpg";
import r4c from "../../assets/img/color-chart/hair-color/r/nclick/4.jpg";

import rb1c from "../../assets/img/color-chart/hair-color/rb/nclick/1.jpg";
import rb2c from "../../assets/img/color-chart/hair-color/rb/nclick/2.jpg";
import rb3c from "../../assets/img/color-chart/hair-color/rb/nclick/3.jpg";
import rb4c from "../../assets/img/color-chart/hair-color/rb/nclick/4.jpg";

import v1c from "../../assets/img/color-chart/hair-color/v/nclick/1.jpg";
import v2c from "../../assets/img/color-chart/hair-color/v/nclick/2.jpg";
import v3c from "../../assets/img/color-chart/hair-color/v/nclick/3.jpg";

import vr1c from "../../assets/img/color-chart/hair-color/vr/nclick/1.jpg";
import vr2c from "../../assets/img/color-chart/hair-color/vr/nclick/2.jpg";

import h1c from "../../assets/img/color-chart/hair-color/100/nclick/1.jpg";
import h2c from "../../assets/img/color-chart/hair-color/100/nclick/2.jpg";
import h3c from "../../assets/img/color-chart/hair-color/100/nclick/3.jpg";
import h4c from "../../assets/img/color-chart/hair-color/100/nclick/4.jpg";
import h5c from "../../assets/img/color-chart/hair-color/100/nclick/5.jpg";
import h6c from "../../assets/img/color-chart/hair-color/100/nclick/6.jpg";
import h7c from "../../assets/img/color-chart/hair-color/100/nclick/7.jpg";
import h8c from "../../assets/img/color-chart/hair-color/100/nclick/8.jpg";
import h9c from "../../assets/img/color-chart/hair-color/100/nclick/9.jpg";
import h10c from "../../assets/img/color-chart/hair-color/100/nclick/10.jpg";

const NalloColorChartScreen = (props) => {
  const [modalContent, setModalContent] = useState("");
  const [open, setOpen] = useState(false);

  useEffect(() => {
    document.title = "Nallo Color Chart";
  });

  const handleModalOpen = (contentNum) => {
    setModalContent(contentNum);
    setOpen(true);
  };

  const handleModalClose = () => {
    setOpen(false);
  };

  let modalBody = (
    <div className="CC-Modal">
      <div>
        <img src={modalContent} alt="" />
      </div>
    </div>
  );

  return (
    <div className="NCC-Main">
      <Modal open={open} onClose={handleModalClose}>
        <Fade in={open} timeout={300}>
          {modalBody}
        </Fade>
      </Modal>
      <div className="CC-Main-Container">
        <div className="CC-Section">
          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T1} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={n1}
                hovImg={n1h}
                onClick={() => {
                  handleModalOpen(n1c);
                }}
              />
              <CCBox
                srcImg={n2}
                hovImg={n2h}
                onClick={() => {
                  handleModalOpen(n2c);
                }}
              />
              <CCBox
                srcImg={n3}
                hovImg={n3h}
                onClick={() => {
                  handleModalOpen(n3c);
                }}
              />
              <CCBox
                srcImg={n4}
                hovImg={n4h}
                onClick={() => {
                  handleModalOpen(n4c);
                }}
              />
              <CCBox
                srcImg={n5}
                hovImg={n5h}
                onClick={() => {
                  handleModalOpen(n5c);
                }}
              />
              <CCBox
                srcImg={n6}
                hovImg={n6h}
                onClick={() => {
                  handleModalOpen(n6c);
                }}
              />
              <CCBox
                srcImg={n7}
                hovImg={n7h}
                onClick={() => {
                  handleModalOpen(n7c);
                }}
              />
              <CCBox
                srcImg={n8}
                hovImg={n8h}
                onClick={() => {
                  handleModalOpen(n8c);
                }}
              />
              <CCBox
                srcImg={n9}
                hovImg={n9h}
                onClick={() => {
                  handleModalOpen(n9c);
                }}
              />
              <CCBox
                srcImg={n10}
                hovImg={n10h}
                onClick={() => {
                  handleModalOpen(n10c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T2} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={a1}
                hovImg={a1h}
                onClick={() => {
                  handleModalOpen(a1c);
                }}
              />
              <CCBox
                srcImg={a2}
                hovImg={a2h}
                onClick={() => {
                  handleModalOpen(a2c);
                }}
              />
              <CCBox
                srcImg={a3}
                hovImg={a3h}
                onClick={() => {
                  handleModalOpen(a3c);
                }}
              />
              <CCBox
                srcImg={a4}
                hovImg={a4h}
                onClick={() => {
                  handleModalOpen(a4c);
                }}
              />
              <CCBox
                srcImg={a6}
                hovImg={a6h}
                onClick={() => {
                  handleModalOpen(a6c);
                }}
              />
              <CCBox
                srcImg={a5}
                hovImg={a5h}
                onClick={() => {
                  handleModalOpen(a5c);
                }}
              />
            </div>
          </div>
          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T3} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={g1}
                hovImg={g1h}
                onClick={() => {
                  handleModalOpen(g1c);
                }}
              />
              <CCBox
                srcImg={g6}
                hovImg={g6h}
                onClick={() => {
                  handleModalOpen(g6c);
                }}
              />
              <CCBox
                srcImg={g2}
                hovImg={g2h}
                onClick={() => {
                  handleModalOpen(g2c);
                }}
              />
              <CCBox
                srcImg={g3}
                hovImg={g3h}
                onClick={() => {
                  handleModalOpen(g3c);
                }}
              />
              <CCBox
                srcImg={g4}
                hovImg={g4h}
                onClick={() => {
                  handleModalOpen(g4c);
                }}
              />
              <CCBox
                srcImg={g5}
                hovImg={g5h}
                onClick={() => {
                  handleModalOpen(g5c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T4} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={gg1}
                hovImg={gg1h}
                onClick={() => {
                  handleModalOpen(gg1c);
                }}
              />
              <CCBox
                srcImg={gg2}
                hovImg={gg2h}
                onClick={() => {
                  handleModalOpen(gg2c);
                }}
              />
              <CCBox
                srcImg={gg3}
                hovImg={gg3h}
                onClick={() => {
                  handleModalOpen(gg3c);
                }}
              />
              <CCBox
                srcImg={gg4}
                hovImg={gg4h}
                onClick={() => {
                  handleModalOpen(gg4c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T5} />
            </div>
            <div className="CC-Row-Container">
              {" "}
              <CCBox
                srcImg={gb1}
                hovImg={gb1h}
                onClick={() => {
                  handleModalOpen(gb1c);
                }}
              />
              <CCBox
                srcImg={gb2}
                hovImg={gb2h}
                onClick={() => {
                  handleModalOpen(gb2c);
                }}
              />
              <CCBox
                srcImg={gb3}
                hovImg={gb3h}
                onClick={() => {
                  handleModalOpen(gb3c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T6} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={b1}
                hovImg={b1h}
                onClick={() => {
                  handleModalOpen(b1c);
                }}
              />
              <CCBox
                srcImg={b2}
                hovImg={b2h}
                onClick={() => {
                  handleModalOpen(b2c);
                }}
              />
              <CCBox
                srcImg={b3}
                hovImg={b3h}
                onClick={() => {
                  handleModalOpen(b3c);
                }}
              />
              <CCBox
                srcImg={b4}
                hovImg={b4h}
                onClick={() => {
                  handleModalOpen(b4c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T7} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={br1}
                hovImg={br1h}
                onClick={() => {
                  handleModalOpen(br1c);
                }}
              />
              <CCBox
                srcImg={br3}
                hovImg={br3h}
                onClick={() => {
                  handleModalOpen(br3c);
                }}
              />
              <CCBox
                srcImg={br2}
                hovImg={br2h}
                onClick={() => {
                  handleModalOpen(br2c);
                }}
              />
              <CCBox
                srcImg={br4}
                hovImg={br4h}
                onClick={() => {
                  handleModalOpen(br4c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T8} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={rb1}
                hovImg={rb1h}
                onClick={() => {
                  handleModalOpen(rb1c);
                }}
              />
              <CCBox
                srcImg={rb2}
                hovImg={rb2h}
                onClick={() => {
                  handleModalOpen(rb2c);
                }}
              />
              <CCBox
                srcImg={rb3}
                hovImg={rb3h}
                onClick={() => {
                  handleModalOpen(rb3c);
                }}
              />
              <CCBox
                srcImg={rb4}
                hovImg={rb4h}
                onClick={() => {
                  handleModalOpen(rb4c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T9} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={kr1}
                hovImg={kr1h}
                onClick={() => {
                  handleModalOpen(kr1c);
                }}
              />
              <CCBox
                srcImg={kr2}
                hovImg={kr2h}
                onClick={() => {
                  handleModalOpen(kr2c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T10} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={k1}
                hovImg={k1h}
                onClick={() => {
                  handleModalOpen(k1c);
                }}
              />
              <CCBox
                srcImg={k2}
                hovImg={k2h}
                onClick={() => {
                  handleModalOpen(k2c);
                }}
              />
              <CCBox
                srcImg={k3}
                hovImg={k3h}
                onClick={() => {
                  handleModalOpen(k3c);
                }}
              />
              <CCBox
                srcImg={k4}
                hovImg={k4h}
                onClick={() => {
                  handleModalOpen(k4c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T11} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={kg7}
                hovImg={kg7h}
                onClick={() => {
                  handleModalOpen(kg7c);
                }}
              />
              <CCBox
                srcImg={kg1}
                hovImg={kg1h}
                onClick={() => {
                  handleModalOpen(kg1c);
                }}
              />
              <CCBox
                srcImg={kg2}
                hovImg={kg2h}
                onClick={() => {
                  handleModalOpen(kg2c);
                }}
              />
              <CCBox
                srcImg={kg3}
                hovImg={kg3h}
                onClick={() => {
                  handleModalOpen(kg3c);
                }}
              />
              <CCBox
                srcImg={kg4}
                hovImg={kg4h}
                onClick={() => {
                  handleModalOpen(kg4c);
                }}
              />
              <CCBox
                srcImg={kg5}
                hovImg={kg5h}
                onClick={() => {
                  handleModalOpen(kg5c);
                }}
              />
              <CCBox
                srcImg={kg6}
                hovImg={kg6h}
                onClick={() => {
                  handleModalOpen(kg6c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T12} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={r1}
                hovImg={r1h}
                onClick={() => {
                  handleModalOpen(r1c);
                }}
              />
              <CCBox
                srcImg={r2}
                hovImg={r2h}
                onClick={() => {
                  handleModalOpen(r2c);
                }}
              />
              <CCBox
                srcImg={r3}
                hovImg={r3h}
                onClick={() => {
                  handleModalOpen(r3c);
                }}
              />
              <CCBox
                srcImg={r4}
                hovImg={r4h}
                onClick={() => {
                  handleModalOpen(r4c);
                }}
              />
            </div>
          </div>

          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T13} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={v1}
                hovImg={v1h}
                onClick={() => {
                  handleModalOpen(v1c);
                }}
              />
              <CCBox
                srcImg={v2}
                hovImg={v2h}
                onClick={() => {
                  handleModalOpen(v2c);
                }}
              />
              <CCBox
                srcImg={v3}
                hovImg={v3h}
                onClick={() => {
                  handleModalOpen(v3c);
                }}
              />
            </div>
          </div>
          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T14} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={vr1}
                hovImg={vr1h}
                onClick={() => {
                  handleModalOpen(vr1c);
                }}
              />
              <CCBox
                srcImg={vr2}
                hovImg={vr2h}
                onClick={() => {
                  handleModalOpen(vr2c);
                }}
              />
            </div>
          </div>
          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T15} />
            </div>
            <div className="CC-Row-Container">
              <CCBox srcImg={ub1} hovImg={ub1h} />
              <CCBox srcImg={ub2} hovImg={ub2h} />
              <CCBox srcImg={ub3} hovImg={ub3h} />
              <CCBox srcImg={ub4} hovImg={ub4h} />
              <CCBox srcImg={ub5} hovImg={ub5h} />
              <CCBox srcImg={ub6} hovImg={ub6h} />
              <CCBox srcImg={ub7} hovImg={ub7h} />
            </div>
          </div>
          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T16} />
            </div>
            <div className="CC-Row-Container">
              <CCBox srcImg={mix1} hovImg={mix1h} />
              <CCBox srcImg={mix2} hovImg={mix2h} />
              <CCBox srcImg={mix3} hovImg={mix3h} />
              <CCBox srcImg={mix4} hovImg={mix4h} />
              <CCBox srcImg={mix5} hovImg={mix5h} />
            </div>
          </div>
          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T17} />
            </div>
            <div className="CC-Row-Container">
              <CCBox
                srcImg={h1}
                hovImg={h1h}
                onClick={() => {
                  handleModalOpen(h1c);
                }}
              />
              <CCBox
                srcImg={h2}
                hovImg={h2h}
                onClick={() => {
                  handleModalOpen(h2c);
                }}
              />
              <CCBox
                srcImg={h3}
                hovImg={h3h}
                onClick={() => {
                  handleModalOpen(h3c);
                }}
              />
              <CCBox
                srcImg={h4}
                hovImg={h4h}
                onClick={() => {
                  handleModalOpen(h4c);
                }}
              />
              <CCBox
                srcImg={h5}
                hovImg={h5h}
                onClick={() => {
                  handleModalOpen(h5c);
                }}
              />
              <CCBox
                srcImg={h6}
                hovImg={h6h}
                onClick={() => {
                  handleModalOpen(h6c);
                }}
              />
              <CCBox
                srcImg={h7}
                hovImg={h7h}
                onClick={() => {
                  handleModalOpen(h7c);
                }}
              />
              <CCBox
                srcImg={h8}
                hovImg={h8h}
                onClick={() => {
                  handleModalOpen(h8c);
                }}
              />
              <CCBox
                srcImg={h9}
                hovImg={h9h}
                onClick={() => {
                  handleModalOpen(h9c);
                }}
              />
              <CCBox
                srcImg={h10}
                hovImg={h10h}
                onClick={() => {
                  handleModalOpen(h10c);
                }}
              />
            </div>
          </div>
          <div className="CC-Row">
            <div className="CC-Row-Container">
              <CCBox srcImg={T18} />
            </div>
            <div className="CC-Row-Container">
              <CCBox srcImg={bc1} hovImg={bc1h} />
              <CCBox />
            </div>
            <div className="CC-Row-Container">
              <CCBox srcImg={T19} />
            </div>
            <div className="CC-Row-Container">
              <CCBox srcImg={mt1} hovImg={mt1h} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NalloColorChartScreen;
