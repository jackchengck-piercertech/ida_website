import React, { useState } from "react";
import "./ColorSystemScreen.css";

import CST from "../../assets/img/color-system/title.svg";
import { Modal } from "@material-ui/core";
import Cross from "../../components/Cross/Cross";
import Footer from "../../components/Footer/Footer";

import N1 from "../../assets/img/color-system/N/1.jpg";
import N2 from "../../assets/img/color-system/N/2.svg";
import N3 from "../../assets/img/color-system/N/3.jpg";

import A1 from "../../assets/img/color-system/A/1.jpg";
import A2 from "../../assets/img/color-system/A/2.svg";
import A3 from "../../assets/img/color-system/A/3.jpg";

import H1 from "../../assets/img/color-system/100/1.jpg";
import H2 from "../../assets/img/color-system/100/2.svg";
import H3 from "../../assets/img/color-system/100/3.jpg";

import G1 from "../../assets/img/color-system/G/1.jpg";
import G2 from "../../assets/img/color-system/G/2.svg";
import G3 from "../../assets/img/color-system/G/3.jpg";

import V1 from "../../assets/img/color-system/V/1.jpg";
import V2 from "../../assets/img/color-system/V/2.svg";
import V3 from "../../assets/img/color-system/V/3.jpg";

import RB1 from "../../assets/img/color-system/RB/1.svg";
import RB2 from "../../assets/img/color-system/RB/2.svg";
import RB3 from "../../assets/img/color-system/RB/3.jpg";

import B1 from "../../assets/img/color-system/B/1.jpg";
import B2 from "../../assets/img/color-system/B/2.svg";
import B3 from "../../assets/img/color-system/B/3.jpg";

import K1 from "../../assets/img/color-system/K/1.jpg";
import K2 from "../../assets/img/color-system/K/2.svg";
import K3 from "../../assets/img/color-system/K/3.jpg";

import KG1 from "../../assets/img/color-system/KG/1.jpg";
import KG2 from "../../assets/img/color-system/KG/2.svg";
import KG3 from "../../assets/img/color-system/KG/3.jpg";

import BR1 from "../../assets/img/color-system/BR/1.jpg";
import BR2 from "../../assets/img/color-system/BR/2.svg";
import BR3 from "../../assets/img/color-system/BR/3.jpg";

import R1 from "../../assets/img/color-system/R/1.jpg";
import R2 from "../../assets/img/color-system/R/2.svg";
import R3 from "../../assets/img/color-system/R/3.jpg";

import MIX1 from "../../assets/img/color-system/MIX/1.jpg";
import MIX2 from "../../assets/img/color-system/MIX/2.svg";
import MIX3 from "../../assets/img/color-system/MIX/3.jpg";

import UB1 from "../../assets/img/color-system/ULTRA-BLOND/1.jpg";
import UB2 from "../../assets/img/color-system/ULTRA-BLOND/2.svg";
import UB3 from "../../assets/img/color-system/ULTRA-BLOND/3.jpg";

import JAM1 from "../../assets/img/color-system/JAM/1.jpg";
import JAM2 from "../../assets/img/color-system/JAM/2.svg";
import JAM3 from "../../assets/img/color-system/JAM/3.jpg";

const ColorSystemScreen = (props) => {
  const [curModal, setCurModal] = useState(0);
  const [modalOpen, setModalOpen] = useState(false);

  const handleModalOpen = (contentNum) => {
    setCurModal(contentNum);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  let modalContent = <div className="CS-Modal-1">This is Modal</div>;

  switch (curModal) {
    case 1:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={N1} alt="" />
          </div>
          <div>
            <img src={N2} alt="" />
            <img src={N3} alt="" />
          </div>
        </div>
      );

      break;
    case 2:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={A1} alt="" />
          </div>
          <div>
            <img src={A2} alt="" />
            <img src={A3} alt="" />
          </div>
        </div>
      );

      break;
    case 3:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={H1} alt="" />
          </div>
          <div>
            <img src={H2} alt="" />
            <img src={H3} alt="" />
          </div>
        </div>
      );

      break;
    case 4:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={G1} alt="" />
          </div>
          <div>
            <img src={G2} alt="" />
            <img src={G3} alt="" />
          </div>
        </div>
      );

      break;
    case 5:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={V1} alt="" />
          </div>
          <div>
            <img src={V2} alt="" />
            <img src={V3} alt="" />
          </div>
        </div>
      );

      break;
    case 6:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={RB1} alt="" />
          </div>
          <div>
            <img src={RB2} alt="" />
            <img src={RB3} alt="" />
          </div>
        </div>
      );

      break;
    case 7:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={B1} alt="" />
          </div>
          <div>
            <img src={B2} alt="" />
            <img src={B3} alt="" />
          </div>
        </div>
      );

      break;
    case 8:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={K1} alt="" />
          </div>
          <div>
            <img src={K2} alt="" />
            <img src={K3} alt="" />
          </div>
        </div>
      );

      break;
    case 9:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={KG1} alt="" />
          </div>
          <div>
            <img src={KG2} alt="" />
            <img src={KG3} alt="" />
          </div>
        </div>
      );

      break;
    case 10:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={BR1} alt="" />
          </div>
          <div>
            <img src={BR2} alt="" />
            <img src={BR3} alt="" />
          </div>
        </div>
      );

      break;
    case 11:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={R1} alt="" />
          </div>
          <div>
            <img src={R2} alt="" />
            <img src={R3} alt="" />
          </div>
        </div>
      );

      break;
    case 12:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={MIX1} alt="" />
          </div>
          <div>
            <img src={MIX2} alt="" />
            <img src={MIX3} alt="" />
          </div>
        </div>
      );

      break;
    case 13:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={UB1} alt="" />
          </div>
          <div>
            <img src={UB2} alt="" />
            <img src={UB3} alt="" />
          </div>
        </div>
      );

      break;
    case 14:
      modalContent = (
        <div className="CS-Modal-1">
          <div>
            <img src={JAM1} alt="" />
          </div>
          <div>
            <img src={JAM2} alt="" />
            <img src={JAM3} alt="" />
          </div>
        </div>
      );

      break;
    default:
      modalContent = <div className="CS-Modal-1">This is Modal</div>;
  }
  return (
    <div className="CS-Main">
      <Modal open={modalOpen} onClose={handleModalClose}>
        <div className="CS-Modal">
          <div className="CS-Modal-Title">
            <img src={CST} alt="" />
            <div className="CS-Modal-Cross">
              <Cross onClick={handleModalClose} />
            </div>
          </div>
          <div className="CS-Modal-Content">{modalContent}</div>

          <div className="CI-Modal-Footer">
            <Footer
              style={{
                paddingTop: "40px",
                borderTop: "1px solid #CCCCCC",
                position: "unset",
                paddingBottom: "30px",
                fontSize: "8px",
              }}
            />
          </div>
        </div>
      </Modal>
      <div className="CS-Title">
        <img src={CST} alt="" />
      </div>
      <div className="CS-Menu">
        <div>
          <div onClick={() => handleModalOpen(1)}>N</div>
          <div onClick={() => handleModalOpen(2)}>A</div>
          <div onClick={() => handleModalOpen(3)}>100</div>
          <div onClick={() => handleModalOpen(4)}>G</div>
          <div onClick={() => handleModalOpen(5)}>V</div>
          <div onClick={() => handleModalOpen(6)}>RB</div>
        </div>
        <div>
          <div onClick={() => handleModalOpen(7)}>B</div>
          <div onClick={() => handleModalOpen(8)}>K</div>
          <div onClick={() => handleModalOpen(9)}>KG</div>
          <div onClick={() => handleModalOpen(10)}>BR</div>
          <div onClick={() => handleModalOpen(11)}>R</div>
          <div onClick={() => handleModalOpen(12)}>MIX</div>
        </div>
        <div>
          <div onClick={() => handleModalOpen(13)}>Ultra Blond</div>
          <div onClick={() => handleModalOpen(14)}>JAM</div>
        </div>
      </div>
    </div>
  );
};

export default ColorSystemScreen;
