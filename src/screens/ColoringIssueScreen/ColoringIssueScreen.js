import React, { useState, useEffect } from "react";
import "./ColoringIssueScreen.css";

import CIT from "../../assets/img/coloring-issue/svg-150064x29.svg";
import { Modal } from "@material-ui/core";
import Footer from "../../components/Footer/Footer";

import cross from "../../assets/img/coloring-issue/cross.svg";
import Cross from "../../components/Cross/Cross";

import in11 from "../../assets/img/coloring-issue/1/1.svg";
import in12 from "../../assets/img/coloring-issue/1/2.svg";

import in21 from "../../assets/img/coloring-issue/2/1.svg";
import in22 from "../../assets/img/coloring-issue/2/2.svg";

import in31 from "../../assets/img/coloring-issue/3/1.svg";
import in32 from "../../assets/img/coloring-issue/3/2.svg";
import in33 from "../../assets/img/coloring-issue/3/3.svg";

import in41 from "../../assets/img/coloring-issue/4/1.svg";
import in42 from "../../assets/img/coloring-issue/4/2.svg";

import in51 from "../../assets/img/coloring-issue/5/1.svg";
import in52 from "../../assets/img/coloring-issue/5/2.svg";
import in53 from "../../assets/img/coloring-issue/5/3.jpg";

import in61 from "../../assets/img/coloring-issue/6/1.svg";
import in62 from "../../assets/img/coloring-issue/6/2.svg";

import in71 from "../../assets/img/coloring-issue/7/1.svg";
import in72 from "../../assets/img/coloring-issue/7/2.svg";

import in81 from "../../assets/img/coloring-issue/8/1.svg";
import in82 from "../../assets/img/coloring-issue/8/2.svg";

import in91 from "../../assets/img/coloring-issue/9/1.svg";
import in92 from "../../assets/img/coloring-issue/9/2.svg";

const mTitle = [
  "Coloring Issue",
  "先了解頭髮",
  "認識染膏",
  "染髮原理",
  "玩盡色彩",
  "AN EASY GUIDE TO PERFECT COLOR!",
  "染膏混合比例及操作時間",
  "塗放方法",
  "如何使用IDA染膏配方調製查閱表",
  "GET IT DONE!",
];

const ColoringIssueScreen = (props) => {
  const [curModal, setCurModal] = useState(0);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    document.title = mTitle[curModal];
  });

  const handleModalOpen = (contentNum) => {
    setCurModal(contentNum);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  let modalContent = <div className="CI-Modal-1">This is Modal</div>;

  switch (curModal) {
    case 1:
      modalContent = (
        <div className="CI-Modal-1">
          <img src={in11} alt="" style={{ width: "570px", height: "578px" }} />
          <img src={in12} alt="" style={{ width: "594px", height: "520px" }} />
        </div>
      );
      break;
    case 2:
      modalContent = (
        <div className="CI-Modal-2">
          <img
            src={in21}
            alt=""
            style={{ width: "832px", height: "216px", marginBottom: "30px" }}
          />
          <img src={in22} alt="" style={{ width: "824px", height: "531px" }} />
        </div>
      );
      break;
    case 3:
      modalContent = (
        <div className="CI-Modal-3">
          <img
            src={in31}
            alt=""
            style={{ width: "1109px", height: "330px", marginBottom: "30px" }}
          />
          <div>
            <div>
              <img
                src={in32}
                alt=""
                style={{ width: "618px", height: "233px" }}
              />
            </div>
            <div>
              <img
                src={in33}
                alt=""
                style={{ width: "458px", height: "294px" }}
              />
            </div>
          </div>
        </div>
      );
      break;
    case 4:
      modalContent = (
        <div className="CI-Modal-4">
          <img
            src={in41}
            alt=""
            style={{ width: "889px", height: "259px", marginBottom: "30px" }}
          />
          <div style={{ padding: "29px", backgroundColor: "black" }}>
            <img
              src={in42}
              alt=""
              style={{ width: "1292px", height: "485px" }}
            />
          </div>
        </div>
      );
      break;

    case 5:
      modalContent = (
        <div className="CI-Modal-5">
          <img
            src={in51}
            alt=""
            style={{ width: "856px", height: "149px", marginBottom: "30px" }}
          />
          <div>
            <img
              src={in52}
              alt=""
              style={{ width: "780px", height: "501px", marginRight: "30px" }}
            />
            <img
              src={in53}
              alt=""
              style={{
                width: "357px",
                height: "501px",
                objectFit: "cover",
                objectPosition: "50% 0",
              }}
            />
          </div>
        </div>
      );

      break;
    case 6:
      modalContent = (
        <div className="CI-Modal-6">
          <img
            src={in61}
            alt=""
            style={{ width: "1147px", height: "291px", marginBottom: "30px" }}
          />
          <img src={in62} alt="" style={{ width: "1342px", height: "488px" }} />
        </div>
      );

      break;
    case 7:
      modalContent = (
        <div className="CI-Modal-7">
          <img
            src={in71}
            alt=""
            style={{ width: "1027px", height: "120px", marginBottom: "80px" }}
          />
          <img src={in72} alt="" style={{ width: "1415px", height: "577px" }} />
        </div>
      );

      break;
    case 8:
      modalContent = (
        <div className="CI-Modal-8">
          <img
            src={in81}
            alt=""
            style={{ width: "1269px", height: "291px", marginBottom: "30px" }}
          />
          <img src={in82} alt="" style={{ width: "1378px", height: "520px" }} />
        </div>
      );

      break;
    case 9:
      modalContent = (
        <div className="CI-Modal-9">
          <img
            src={in91}
            alt=""
            style={{ width: "932px", height: "211px", marginBottom: "30px" }}
          />
          <img src={in92} alt="" style={{ width: "1356px", height: "518px" }} />
        </div>
      );

      break;
    default:
      modalContent = <div className="CI-Modal-1">This is Modal</div>;
  }
  return (
    <div className="CI-Main">
      <Modal open={modalOpen} onClose={handleModalClose}>
        <div className="CI-Modal">
          <div className="CI-Modal-Title">
            <img src={CIT} alt="" />
            <div className="CI-Modal-Cross">
              <Cross onClick={handleModalClose} />
            </div>
          </div>
          <div className="CI-Modal-Content">{modalContent}</div>
          <div className="CI-Modal-Footer">
            <Footer
              style={{
                paddingTop: "40px",
                borderTop: "1px solid #CCCCCC",
                position: "unset",
                paddingBottom: "30px",
                fontSize: "8px",
              }}
            />
          </div>
        </div>
      </Modal>
      <div className="CI-Title">
        <img src={CIT} alt="" />
      </div>
      <div className="CI-Menu">
        <ul>
          <li>
            <div onClick={() => handleModalOpen(1)}>先了解頭髮</div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(2)}>認識染膏</div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(3)}>染髮原理</div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(4)}>玩盡色彩</div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(5)}>
              AN EASY GUIDE TO PERFECT COLOR!
            </div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(6)}>染膏混合比例及操作時間</div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(7)}>塗放方法</div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(8)}>
              如何使用IDA染膏配方調製查閱表
            </div>
          </li>
          <li>
            <div onClick={() => handleModalOpen(9)}>GET IT DONE!</div>
          </li>
        </ul>
      </div>
      <Footer className="white" />
    </div>
  );
};

export default ColoringIssueScreen;
