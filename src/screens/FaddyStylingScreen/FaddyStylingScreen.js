import React, { useState, useEffect } from "react";
import "./FaddyStylingScreen.css";
import FV from "../../components/FV/FV";
import Footer from "../../components/Footer/Footer";
import { Fade, Modal } from "@material-ui/core";

import f11 from "../../assets/img/faddy-styling/waxy cream front.jpg";
import f12 from "../../assets/img/faddy-styling/wet look front.jpg";
import f13 from "../../assets/img/faddy-styling/bouncy waxy cream front.jpg";

import f21 from "../../assets/img/faddy-styling/big1-u22018-r-fr.png";

import f221 from "../../assets/img/faddy-styling/hair spray front.jpg";
import f222 from "../../assets/img/faddy-styling/moving spray front.jpg";
import f223 from "../../assets/img/faddy-styling/muss n hold front.jpg";
import f224 from "../../assets/img/faddy-styling/waxy clay front.jpg";

import f31 from "../../assets/img/faddy-styling/sea salt spray front.jpg";
import f32 from "../../assets/img/faddy-styling/molding clay front.jpg";
import f33 from "../../assets/img/faddy-styling/soft wave mousse front.jpg";

// import fb11 from "../../assets/img/faddy-styling/waxy cream back.jpg";
// import fb12 from "../../assets/img/faddy-styling/wet look back.jpg";
// import fb13 from "../../assets/img/faddy-styling/bouncy waxy cream back.jpg";

// import fb21 from "../../assets/img/faddy-styling/big1-u22018-r-fr.png";

// import fb221 from "../../assets/img/faddy-styling/hair spray back.jpg";
// import fb222 from "../../assets/img/faddy-styling/moving spray back.jpg";
// import fb223 from "../../assets/img/faddy-styling/muss n hold back.jpg";
// import fb224 from "../../assets/img/faddy-styling/waxy clay back.jpg";

// import fb31 from "../../assets/img/faddy-styling/sea salt spray back.jpg";
// import fb32 from "../../assets/img/faddy-styling/molding clay back.jpg";
// import fb33 from "../../assets/img/faddy-styling/soft wave mousse back.jpg";

import in1 from "../../assets/img/faddy-styling/1.svg";
import in2 from "../../assets/img/faddy-styling/2.svg";
import in3 from "../../assets/img/faddy-styling/3.svg";
import in5 from "../../assets/img/faddy-styling/5.svg";
import in6 from "../../assets/img/faddy-styling/6.svg";
import in7 from "../../assets/img/faddy-styling/7.svg";
import in8 from "../../assets/img/faddy-styling/8.svg";
import in9 from "../../assets/img/faddy-styling/9.svg";
import in10 from "../../assets/img/faddy-styling/10.svg";
import in11 from "../../assets/img/faddy-styling/11.svg";

const FaddyStylingScreen = (props) => {
  const [curModal, setCurModal] = useState(0);
  const [modalOpen, setModalOpen] = useState(false);
  useEffect(() => {
    document.title = "Faddy Styling";
  });

  const handleModalOpen = (contentNum) => {
    setCurModal(contentNum);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  let modalContent = <div className="FS-Modal-1">This is Modal</div>;

  switch (curModal) {
    case 1:
      modalContent = (
        <div className="FS-Modal-1">
          <img src={in1} alt="" />
        </div>
      );

      break;
    case 2:
      modalContent = (
        <div className="FS-Modal-2">
          <img src={in2} alt="" />
        </div>
      );

      break;
    case 3:
      modalContent = (
        <div className="FS-Modal-3">
          <img src={in3} alt="" />
        </div>
      );

      break;
    case 4:
      modalContent = (
        <div className="FS-Modal-4">{/* <img src={f21} alt="" /> */}</div>
      );

      break;
    case 5:
      modalContent = (
        <div className="FS-Modal-5">
          <img src={in5} alt="" />
        </div>
      );

      break;
    case 6:
      modalContent = (
        <div className="FS-Modal-6">
          <img src={in6} alt="" />
        </div>
      );

      break;
    case 7:
      modalContent = (
        <div className="FS-Modal-7">
          <img src={in7} alt="" />
        </div>
      );

      break;
    case 8:
      modalContent = (
        <div className="FS-Modal-8">
          <img src={in8} alt="" />
        </div>
      );

      break;
    case 9:
      modalContent = (
        <div className="FS-Modal-9">
          <img src={in9} alt="" />
        </div>
      );

      break;
    case 10:
      modalContent = (
        <div className="FS-Modal-10">
          <img src={in10} alt="" />
        </div>
      );

      break;
    case 11:
      modalContent = (
        <div className="FS-Modal-11">
          <img src={in11} alt="" />
        </div>
      );
      break;

    default:
      modalContent = <div className="FS-Modal-1">This is Modal</div>;
  }

  return (
    <div className="FS-Main">
      <Modal open={modalOpen} onClose={handleModalClose}>
        <Fade in={modalOpen} timeout={500}>
          <div className="FS-Modal">
            {modalContent}
            <div className="Modal-Close" onClick={() => handleModalClose()}>
              <div></div>
            </div>
          </div>
        </Fade>
      </Modal>
      <div className="FS-Main-MainContainer">
        <FV pT="52%">
          <div className="FS-Main-Container">
            <div className="FS-Column1">
              <div>
                <FV pT="52%">
                  <div className="FS-Box"></div>
                </FV>
              </div>
              <div>
                <FV pT="52%">
                  <div className="FS-Box" onClick={() => handleModalOpen(1)}>
                    <img src={f11} alt="" />
                  </div>
                </FV>
              </div>
              <div>
                <FV pT="52%">
                  <div className="FS-Box" onClick={() => handleModalOpen(2)}>
                    <img src={f12} alt="" />
                  </div>
                </FV>
              </div>
              <div>
                <FV pT="52%">
                  <div className="FS-Box" onClick={() => handleModalOpen(3)}>
                    <img src={f13} alt="" />
                  </div>
                </FV>
              </div>
            </div>
            <div className="FS-Column2">
              <div className="FS-Column2-Container">
                <FV pT="52%">
                  <div className="FS-Column-2-1">
                    <FV pT="52%">
                      <div
                        className="FS-Box"
                        onClick={() => handleModalOpen(4)}
                      >
                        <img src={f21} alt="" />
                      </div>
                    </FV>
                  </div>
                </FV>
              </div>
              <div className="FS-Column2-Container">
                <FV pT="52%">
                  <div className="FS-Column2-4">
                    <div>
                      <div>
                        <FV pT="52%">
                          <div
                            className="FS-Box"
                            onClick={() => handleModalOpen(5)}
                          >
                            <img src={f221} alt="" />
                          </div>
                        </FV>
                      </div>
                      <div>
                        <FV pT="52%">
                          <div
                            className="FS-Box"
                            onClick={() => handleModalOpen(6)}
                          >
                            <img src={f222} alt="" />
                          </div>
                        </FV>
                      </div>
                    </div>
                    <div>
                      <div>
                        <FV pT="52%">
                          <div
                            className="FS-Box"
                            onClick={() => handleModalOpen(7)}
                          >
                            <img src={f223} alt="" />
                          </div>
                        </FV>
                      </div>
                      <div>
                        <FV pT="52%">
                          <div
                            className="FS-Box"
                            onClick={() => handleModalOpen(8)}
                          >
                            <img src={f224} alt="" />
                          </div>
                        </FV>
                      </div>
                    </div>
                  </div>
                </FV>
              </div>
            </div>
            <div className="FS-Column1">
              <div>
                <FV pT="52%">
                  <div className="FS-Box"></div>
                </FV>
              </div>
              <div>
                <FV pT="52%">
                  <div className="FS-Box" onClick={() => handleModalOpen(9)}>
                    <img src={f31} alt="" />
                  </div>
                </FV>
              </div>
              <div>
                <FV pT="52%">
                  <div className="FS-Box" onClick={() => handleModalOpen(10)}>
                    <img src={f32} alt="" />
                  </div>
                </FV>
              </div>
              <div>
                <FV pT="52%">
                  <div className="FS-Box" onClick={() => handleModalOpen(11)}>
                    <img src={f33} alt="" />
                  </div>
                </FV>
              </div>
            </div>
          </div>
        </FV>
      </div>
      <div className="FS-Main-MobileContainer">
        <div className="FS-Main-Container">
          <div className="FS-ColumnFull">
            <div className="FS-Column2-Container">
              <FV pT="52%">
                <div className="FS-Column-2-1">
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(4)}>
                      <img src={f21} alt="" />
                    </div>
                  </FV>
                </div>
              </FV>
            </div>
            <div className="FS-Column2-Container">
              <div className="FS-Column2-2">
                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(9)}>
                      <img src={f31} alt="" />
                    </div>
                  </FV>
                </div>
                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(1)}>
                      <img src={f11} alt="" />
                    </div>
                  </FV>
                </div>

                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(5)}>
                      <img src={f221} alt="" />
                    </div>
                  </FV>
                </div>
                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(10)}>
                      <img src={f32} alt="" />
                    </div>
                  </FV>
                </div>

                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(2)}>
                      <img src={f12} alt="" />
                    </div>
                  </FV>
                </div>
                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(3)}>
                      <img src={f13} alt="" />
                    </div>
                  </FV>
                </div>

                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(7)}>
                      <img src={f223} alt="" />
                    </div>
                  </FV>
                </div>
                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(6)}>
                      <img src={f222} alt="" />
                    </div>
                  </FV>
                </div>

                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(11)}>
                      <img src={f33} alt="" />
                    </div>
                  </FV>
                </div>
                <div>
                  <FV pT="52%">
                    <div className="FS-Box" onClick={() => handleModalOpen(8)}>
                      <img src={f224} alt="" />
                    </div>
                  </FV>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer style={{ fontSize: "10px", marginBottom: "15px" }} />
    </div>
  );
};

export default FaddyStylingScreen;
