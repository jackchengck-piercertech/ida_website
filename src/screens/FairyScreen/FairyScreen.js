import React, { useEffect } from "react";
import "./FairyScreen.css";
import Slider from "react-slick";

import bannerL from "../../assets/img/fairy/fairy girl2.png";
import bannerR from "../../assets/img/fairy/svg-389592x186.svg";

import p1 from "../../assets/img/fairy/ktt spray.png";
import p2 from "../../assets/img/fairy/ktt treatment.png";
import p3 from "../../assets/img/fairy/cf shampoo png.png";
import p4 from "../../assets/img/fairy/hr shampoo png.png";

import pi1 from "../../assets/img/fairy/svg-247546x1662.svg";
import pi2 from "../../assets/img/fairy/svg-247546x166.svg";
import pi3 from "../../assets/img/fairy/svg-262942x158.svg";
import pi4 from "../../assets/img/fairy/svg-236942x177.svg";

import motto from "../../assets/img/fairy/fairy text 3.png";
import FV from "../../components/FV/FV";
import MoreDetail from "../../assets/img/fairy/svg 61271x9_poster_.png";
import AnchorLink from "react-anchor-link-smooth-scroll";
import Footer from "../../components/Footer/Footer";
import DropletButton from "../../components/DropletButton/DropletButton";
import topButton from "../../assets/img/nallo-color/svg 39x39_poster_.png";

import l1l from "../../assets/img/fairy/lc/1l.png";
import l2l from "../../assets/img/fairy/lc/2l.png";
import l3l from "../../assets/img/fairy/lc/3l.png";
import l1r from "../../assets/img/fairy/lc/1r.svg";
import l2r from "../../assets/img/fairy/lc/2r.svg";
import l3r from "../../assets/img/fairy/lc/3r.svg";
import lt1 from "../../assets/img/fairy/lc/t1.svg";
import lt2 from "../../assets/img/fairy/lc/t2.svg";
import lt3 from "../../assets/img/fairy/lc/t3.svg";

import r1l from "../../assets/img/fairy/rc/1l.png";
import r2l from "../../assets/img/fairy/rc/2l.png";
import r3l from "../../assets/img/fairy/rc/3l.png";
import r1r from "../../assets/img/fairy/rc/1r.svg";
import r2r from "../../assets/img/fairy/rc/2r.svg";
import r3r from "../../assets/img/fairy/rc/3r.svg";
import rt1 from "../../assets/img/fairy/rc/t1.svg";
import rt2 from "../../assets/img/fairy/rc/t2.svg";
import rt3 from "../../assets/img/fairy/rc/t3.svg";

const FairyScreen = (props) => {
  useEffect(() => {
    document.title = "Fairy";
  });

  const lct = [lt1, lt2, lt3];
  const rct = [rt1, rt2, rt3];

  const lcSettings = {
    customPaging: function (i) {
      return (
        <a>
          <img className="Fairy-Slider-Button" src={lct[i]} alt="" />
        </a>
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  const rcSettings = {
    customPaging: function (i) {
      return (
        <a>
          <img className="Fairy-Slider-Button" src={rct[i]} alt="" />
        </a>
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div className="Fairy-Main">
      <div className="Fairy-Banner" id="fairyBanner">
        <div className="Fairy-Banner-Container">
          <img
            src={bannerL}
            alt=""
            style={{
              height: "100%",
              objectFit: "contain",
            }}
          />
          <img src={bannerR} alt="" style={{ width: "30%" }} />
        </div>
      </div>
      <div className="Fairy-Product">
        <div className="Fairy-Product-Container">
          <div>
            <div className="Fairy-Product-Box">
              <img src={p1} alt="" style={{ width: "80px" }} />
              <img src={pi1} alt="" style={{ width: "260px" }} />
            </div>
          </div>
          <div>
            <div className="Fairy-Product-Box">
              <img src={p2} alt="" style={{ width: "100px", height: "100%" }} />
              <div style={{ paddingLeft: "2%", paddingRight: "2%" }}>
                <div className="Fairy-MoreDetail">
                  <a href="/ktt-details">
                    <img
                      src={MoreDetail}
                      alt=""
                      // style={{ height: "1rem", width: "auto" }}
                    />
                  </a>
                </div>
                <img src={pi2} alt="" style={{ width: "259px" }} />
              </div>
            </div>
          </div>
          <div>
            <div className="Fairy-Product-Box">
              <img src={p3} alt="" style={{ width: "145px", height: "90%" }} />
              <img src={pi3} alt="" style={{ width: "275px" }} />
            </div>
          </div>
          <div>
            <div className="Fairy-Product-Box">
              <img src={p4} alt="" style={{ width: "101px", height: "100%" }} />
              <img src={pi4} alt="" style={{ width: "247px" }} />
            </div>
          </div>
        </div>
      </div>

      <div className="Fairy-Motto">
        <img src={motto} alt="" />
      </div>

      <div className="Fairy-Slider">
        <div className="Fairy-Slider-Section">
          <div className="Fairy-Slider-Container">
            <Slider {...lcSettings}>
              <div>
                <div className="Fairy-Slider-Img Fairy-Slider-Img-L">
                  <img
                    src={l1l}
                    alt=""
                    style={{ width: "198px", height: "286px" }}
                  />
                  <img src={l1r} alt="" />
                </div>
              </div>
              <div>
                <div className="Fairy-Slider-Img Fairy-Slider-Img-L">
                  <img
                    src={l2l}
                    alt=""
                    style={{ width: "198px", height: "286px" }}
                  />
                  <img src={l2r} alt="" />
                </div>
              </div>
              <div>
                <div className="Fairy-Slider-Img Fairy-Slider-Img-L">
                  <img
                    src={l3l}
                    alt=""
                    style={{ width: "198px", height: "286px" }}
                  />
                  <img src={l3r} alt="" />
                </div>
              </div>
            </Slider>
          </div>
        </div>
        <div className="Fairy-Slider-Section">
          <div className="Fairy-Slider-Container">
            <Slider {...rcSettings}>
              <div>
                <div className="Fairy-Slider-Img Fairy-Slider-Img-R">
                  <img
                    src={r1l}
                    alt=""
                    style={{ width: "191px", height: "302px" }}
                  />
                  <img src={r1r} alt="" />
                </div>
              </div>
              <div>
                <div className="Fairy-Slider-Img Fairy-Slider-Img-R">
                  <img
                    src={r2l}
                    alt=""
                    style={{ width: "191px", height: "302px" }}
                  />
                  <img src={r2r} alt="" />
                </div>
              </div>
              <div>
                <div className="Fairy-Slider-Img Fairy-Slider-Img-R">
                  <img
                    src={r3l}
                    alt=""
                    style={{ width: "191px", height: "302px" }}
                  />
                  <img src={r3r} alt="" />
                </div>
              </div>
            </Slider>
          </div>
        </div>
      </div>

      <div className="Fairy-TopButton">
        <AnchorLink href="#fairyBanner">
          <img src={topButton} alt="" />
        </AnchorLink>
      </div>
      <Footer style={{ fontSize: "10px", marginBottom: "20px" }} />
    </div>
  );
};

export default FairyScreen;
