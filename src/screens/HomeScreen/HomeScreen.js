import React, { useState } from "react";
import AnchorLink from "react-anchor-link-smooth-scroll";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import Tabs from "../../components/Tab/Tabs";
import Tab from "../../components/Tab/Tab";
// import Tabs from "@material-ui/core/Tabs";
// import Tab from "@material-ui/core/Tab";

import Ghost from "../../components/Ghost/Ghost";
import Center from "../../components/Center/Center";
// import TabPanel from "../../components/TabPanel";
import Footer from "../../components/Footer/Footer";

import logo from "../../assets/img/index/logo_ida.svg";
import downarrow from "../../assets/img/index/downarrow-svg-35x35.svg";
import brandicon1 from "../../assets/img/index/brand_icon/brand icon 1.jpg";
import brandicon2 from "../../assets/img/index/brand_icon/brand icon 2.jpg";
import brandicon3 from "../../assets/img/index/brand_icon/brand icon 3(faddy).jpg";
import brandicon4 from "../../assets/img/index/brand_icon/brand icon 4(fairy).jpg";
import brandicon5 from "../../assets/img/index/brand_icon/brand icon 5.jpg";
import brandicon6 from "../../assets/img/index/brand_icon/brand icon 6.jpg";
import brandicon7 from "../../assets/img/index/brand_icon/brand icon 7.jpg";
import brandicon8 from "../../assets/img/index/brand_icon/brand icon 8.jpg";
import brandicon9 from "../../assets/img/index/brand_icon/brand icon 9(basic).jpg";
import quotationMark from "../../assets/img/index/quote.png";
import sp1 from "../../assets/img/index/s_p/01 faddy color b.jpg";
import sp2 from "../../assets/img/index/s_p/06 bouncy b.jpg";
import sp3 from "../../assets/img/index/s_p/02 b.jpg";
import sp4 from "../../assets/img/index/s_p/5-buttonu51978-fr.jpg";
import sp5 from "../../assets/img/index/s_p/07 b.jpg";
import sp6 from "../../assets/img/index/s_p/04 b.jpg";
import sp7 from "../../assets/img/index/s_p/08 b.jpg";
import sp8 from "../../assets/img/index/s_p/03 b.jpg";
import sp1_bg from "../../assets/img/index/s_p/web design4-01.jpg";
import sp2_bg from "../../assets/img/index/s_p/web design4-04.jpg";
import sp3_bg from "../../assets/img/index/s_p/web design4-03.jpg";
import sp4_bg from "../../assets/img/index/s_p/web design4-06.jpg";
import sp5_bg from "../../assets/img/index/s_p/web design4-02.jpg";
import sp6_bg from "../../assets/img/index/s_p/web design4-08.jpg";
import sp7_bg from "../../assets/img/index/s_p/web design4-05.jpg";
import sp8_bg from "../../assets/img/index/s_p/web design4-07.jpg";
import cimg1 from "../../assets/img/index/web page-29-u17657-fr.png";
import cimg2 from "../../assets/img/index/web page-30-u17651-fr.png";
import cimg3 from "../../assets/img/index/web page-31-u17652-fr.png";
import cimg4 from "../../assets/img/index/web page-32-u17653-fr.png";
import c0 from "../../assets/img/index/carousel/0.jpg";
import c1 from "../../assets/img/index/carousel/1.jpg";
import c2 from "../../assets/img/index/carousel/2.jpg";
import c3 from "../../assets/img/index/carousel/3.jpg";
import c4 from "../../assets/img/index/carousel/4.jpg";
import c5 from "../../assets/img/index/carousel/5.jpg";
import c6 from "../../assets/img/index/carousel/6.jpg";
import c7 from "../../assets/img/index/carousel/7.jpg";
import c8 from "../../assets/img/index/carousel/8.jpg";
import c9 from "../../assets/img/index/carousel/9.jpg";
import poster from "../../assets/img/index/new_2020_1.svg";
import fbIcon from "../../assets/img/index/fb icon2.png";
import ytIcon from "../../assets/img/index/you tube.png";
import wcIcon from "../../assets/img/index/wechat.png";
import igIcon from "../../assets/img/index/ig-icon.png";
import buyIcon from "../../assets/img/index/buyonline_icon-03.png";
import upTop from "../../assets/img/index/up.svg";

import wcQr from "../../assets/img/index/ida wechart qrcode.jpg";

import "./HomeScreen.scss";
import FV from "../../components/FV/FV";
import { Modal } from "@material-ui/core";
import { Crop169, Crop54 } from "@material-ui/icons";

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const HomeScreen = (props) => {
  const [openTab, setOpenTab] = useState(0);
  const [name, setName] = useState("");
  const [areaCode, setAreaCode] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [curModal, setCurModal] = useState(0);
  const [modalOpen, setModalOpen] = useState(false);

  const handleTabChange = (newValue) => {
    setOpenTab(newValue);
  };

  const handleFormSubmit = (evt) => {
    evt.preventDefault();
    console.log(name, areaCode, phone, email, message);
  };

  const handleModalOpen = (contentNum) => {
    setCurModal(contentNum);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  let form = (
    <form className="Index-FormTab-Form" onSubmit={handleFormSubmit}>
      <div className="Index-FormTab-FormGrp">
        <label>Name:</label>
        <input
          type="text"
          placeholder="Enter Name"
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div className="Index-FormTab-FormGrp" style={{}}>
        <div
          style={{
            width: "30%",
            display: "inline-block",
            paddingRight: "2%",
          }}
        >
          <label>Area Code:</label>
          <input
            type="text"
            placeholder="Enter Area Code"
            name="area_code"
            pattern="[0-9]{3}"
            value={areaCode}
            onChange={(e) => setAreaCode(e.target.value)}
          />
        </div>
        <div style={{ width: "70%", display: "inline-block" }}>
          <label>Phone:</label>
          <input
            type="tel"
            placeholder="Enter Phone"
            name="phone"
            pattern="[0-9]{8}"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
        </div>
      </div>
      <div className="Index-FormTab-FormGrp">
        <label>Email:</label>
        <input
          type="email"
          placeholder="Enter Email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div className="Index-FormTab-FormGrp">
        <label>Message:</label>
        <textarea
          name="message"
          placeholder="Enter Message"
          rows="3"
          onChange={(e) => setMessage(e.target.value)}
        >
          {message}
        </textarea>
      </div>
      <div className="Index-FormTab-FormGrp">
        <input type="submit" />
      </div>
    </form>
  );

  let formDescription = (
    <div className="Index-FormTab-Product-Description">
      <p>IDA營銷各式專業美髮用品系列，歡迎您與我們聯絡，獲取更多產品資訊。</p>
    </div>
  );

  if (openTab === 1) {
    form = (
      <form className="Index-FormTab-Form" onSubmit={handleFormSubmit}>
        <div className="Index-FormTab-FormGrp">
          <label>Name:</label>
          <input
            type="text"
            placeholder="Enter Name"
            name="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className="Index-FormTab-FormGrp" style={{}}>
          <div
            style={{
              width: "30%",
              display: "inline-block",
              paddingRight: "2%",
            }}
          >
            <label>Area Code:</label>
            <input
              type="text"
              placeholder="Enter Area Code"
              name="area_code"
              pattern="[0-9]{3}"
              value={areaCode}
              onChange={(e) => setAreaCode(e.target.value)}
            />
          </div>
          <div style={{ width: "70%", display: "inline-block" }}>
            <label>Phone:</label>
            <input
              type="tel"
              placeholder="Enter Phone"
              name="phone"
              pattern="[0-9]{8}"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
          </div>
        </div>
        <div className="Index-FormTab-FormGrp">
          <label>Email:</label>
          <input
            type="email"
            placeholder="Enter Email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="Index-FormTab-FormGrp">
          <label>Message:</label>
          <textarea
            name="message"
            placeholder="Enter Message"
            rows="3"
            onChange={(e) => setMessage(e.target.value)}
          >
            {message}
          </textarea>
        </div>
        <div className="Index-FormTab-FormGrp">
          <input type="submit" />
        </div>
      </form>
    );
    formDescription = (
      <div className="Index-FormTab-Coop-Description">
        <p>
          IDA營銷各式專業美髮用品系列，客戶遍布中國、香港及東南亞。我們尋求不同形式的合作，歡迎您與我們聯絡，成為我們的合作伙伴。
        </p>
        <p>
          銷售及批發查詢：<br></br> 歡迎各批發商及零售商提供營銷合作機會
        </p>
        <p>
          媒體及市場推廣查詢：<br></br>
          歡迎各KOL、髮型師、美容達人、媒體及團體機構提供推廣合作機會
        </p>
      </div>
    );
  }

  let modalContent = <div className="Index-Modal-1">This is Modal</div>;

  switch (curModal) {
    case 1:
      modalContent = (
        <div className="Index-Modal-1">
          <img src={wcQr} alt="" />
        </div>
      );
      break;

    default:
      modalContent = <div className="Index-Modal-1">This is Modal</div>;
  }

  return (
    <div className="Index-Main">
      <Modal open={modalOpen} onClose={handleModalClose}>
        <div className="Index-Modal">{modalContent}</div>
      </Modal>
      <div className="Index-LogoSection" id="frontpageTop">
        <FV pT="62.2%">
          <Center containerStyle="Index-CenterBox">
            <div className="Index-LogoBox">
              <img src={logo} className="Index-Logo" alt="logo" />
            </div>

            <div
              className="Index-Animation-Motto"
              style={{ paddingBottom: "200px" }}
            >
              <Ghost timer={5000} timeC={3} active={0}>
                We work for professional
              </Ghost>
              <Ghost timer={5000} timeC={3} active={1}>
                Since 1982
              </Ghost>
              <Ghost
                timer={5000}
                timeC={3}
                active={2}
                style={{ fontSize: "2.7rem" }}
              >
                專業之選・唯美之道
              </Ghost>
            </div>
            <div>
              <AnchorLink
                href="#frontpagebrandicon"
                className="Index-Downarrow"
              >
                <img
                  src={downarrow}
                  alt="down Arrow"
                  style={{ width: "3rem" }}
                />
              </AnchorLink>
            </div>
          </Center>
        </FV>
      </div>
      <div className="Index-LinksSection" id="frontpagebrandicon">
        <Center>
          <FV className="Index-LinksSection-Container" pT={"83.73%"}>
            <div className="Index-BrandContainer">
              <div className="Index-BrandContainer-Row">
                <div className="Index-BrandContainer-RGrid">
                  <div className="Index-BrandContainer-Grid">
                    <a href="/ionic" className="no-hover">
                      <img src={brandicon1} alt="Brand Icon" />
                    </a>
                  </div>
                  <div className="Index-BrandContainer-Grid">
                    <a href="/ionic">
                      <img src={brandicon2} alt="Brand Icon" />
                    </a>
                  </div>
                </div>
                <div className="Index-BrandContainer-RGrid">
                  <a href="/faddy-styling">
                    <img src={brandicon3} alt="Brand Icon" />
                  </a>
                </div>
              </div>
              <div className="Index-BrandContainer-Row">
                <div className="Index-BrandContainer-RGrid">
                  <a href="/fairy">
                    <img src={brandicon4} alt="Brand Icon" />
                  </a>
                </div>
                <div className="Index-BrandContainer-RGrid">
                  <div className="Index-BrandContainer-Grid">
                    <a href="/nallo-color" className="no-hover">
                      <img src={brandicon5} alt="Brand Icon" />
                    </a>
                  </div>
                  <div className="Index-BrandContainer-Grid">
                    <a href="/nallo-color">
                      <img src={brandicon6} alt="Brand Icon" />
                    </a>
                  </div>
                </div>
              </div>
              <div className="Index-BrandContainer-Row">
                <div className="Index-BrandContainer-RGrid">
                  <div className="Index-BrandContainer-Grid">
                    <img src={brandicon7} alt="Brand Icon" />
                  </div>
                  <div className="Index-BrandContainer-Grid">
                    <a href="/xssential-color">
                      <img src={brandicon8} alt="Brand Icon" />
                    </a>
                  </div>
                </div>
                <div className="Index-BrandContainer-RGrid">
                  <a href="/basic-color">
                    <img src={brandicon9} alt="Brand Icon" />
                  </a>
                </div>
              </div>
            </div>
          </FV>
        </Center>
      </div>
      <div className="Index-MottoSection">
        <Center>
          <div className="Index-Motto-Image">
            <img src={quotationMark} alt="Quotation Mark" />
          </div>
          <div className="Index-Animation-Motto">
            <Ghost timer={5000} timeC={3} active={0}>
              The best color is the one that looks good on you
            </Ghost>
            <Ghost timer={5000} timeC={3} active={1}>
              We look for beauty in everything we do
            </Ghost>
            <Ghost
              timer={5000}
              timeC={3}
              active={2}
              style={{ fontSize: "2.7rem" }}
            >
              演繹自我本色 成就創作之旅
            </Ghost>
          </div>
        </Center>
      </div>
      <div className="Index-SPSection">
        <div className="Index-SPContainer">
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <div>
                <img
                  src={sp1}
                  alt=""
                  className="Index-SPContainer-Front"
                  style={{ zIndex: "10" }}
                />
                <img src={sp1_bg} alt="" className="Index-SPContainer-Back" />
              </div>
            </FV>
          </div>
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <img
                src={sp2}
                alt=""
                className="Index-SPContainer-Front"
                style={{ zIndex: "10" }}
              />
              <img src={sp2_bg} alt="" className="Index-SPContainer-Back" />
            </FV>
          </div>
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <img
                src={sp3}
                alt=""
                className="Index-SPContainer-Front"
                style={{ zIndex: "10" }}
              />
              <img src={sp3_bg} alt="" className="Index-SPContainer-Back" />
            </FV>
          </div>
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <img
                src={sp4}
                alt=""
                className="Index-SPContainer-Front"
                style={{ zIndex: "10" }}
              />
              <img src={sp4_bg} alt="" className="Index-SPContainer-Back" />
            </FV>
          </div>
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <img
                src={sp5}
                alt=""
                className="Index-SPContainer-Front"
                style={{ zIndex: "10" }}
              />
              <img src={sp5_bg} alt="" className="Index-SPContainer-Back" />
            </FV>
          </div>
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <img
                src={sp6}
                alt=""
                className="Index-SPContainer-Front"
                style={{ zIndex: "10" }}
              />
              <img src={sp6_bg} alt="" className="Index-SPContainer-Back" />
            </FV>
          </div>
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <img
                src={sp7}
                alt=""
                className="Index-SPContainer-Front"
                style={{ zIndex: "10" }}
              />
              <img src={sp7_bg} alt="" className="Index-SPContainer-Back" />
            </FV>
          </div>
          <div className="Index-SPContainer-Grid">
            <FV pT={"100%"}>
              <img
                src={sp8}
                alt=""
                className="Index-SPContainer-Front"
                style={{ zIndex: "10" }}
              />
              <img src={sp8_bg} alt="" className="Index-SPContainer-Back" />
            </FV>
          </div>
        </div>
      </div>
      <div className="Index-MottoSection Index-CharacteristicSection">
        <Center>
          <div className="Index-CharacteristicSection-Container">
            <div className="Index-CharacteristicSection-Grid">
              <div className="Index-CharacteristicSection-Grid-Title">
                Shampoo & Treatment
              </div>
              <div>
                <img src={cimg1} alt="" />
              </div>
            </div>
            <div className="Index-CharacteristicSection-Grid">
              <div className="Index-CharacteristicSection-Grid-Title">
                Perm & Straighten
              </div>
              <div>
                <img src={cimg2} alt="" />
              </div>
            </div>
            <div className="Index-CharacteristicSection-Grid">
              <div className="Index-CharacteristicSection-Grid-Title">
                Hair Colors
              </div>
              <div>
                <img src={cimg3} alt="" />
              </div>
            </div>
            <div className="Index-CharacteristicSection-Grid">
              <div className="Index-CharacteristicSection-Grid-Title">
                Styling
              </div>
              <div>
                <img src={cimg4} alt="" />
              </div>
            </div>
          </div>
        </Center>
      </div>
      <div className="Index-CarouselSection">
        <Carousel
          showArrows={false}
          showThumbs={false}
          showIndicators={false}
          showStatus={false}
          autoPlay={true}
          infiniteLoop={true}
          swipeable={true}
          dynamicHeight={true}
        >
          <div>
            <img src={c1} alt="" />
          </div>
          <div>
            <img src={c2} alt="" />
          </div>
          <div>
            <img src={c3} alt="" />
          </div>
          <div>
            <img src={c4} alt="" />
          </div>
          <div>
            <img src={c5} alt="" />
          </div>
          <div>
            <img src={c6} alt="" />
          </div>
          <div>
            <img src={c7} alt="" />
          </div>
          <div>
            <img src={c8} alt="" />
          </div>
          <div>
            <img src={c9} alt="" />
          </div>
        </Carousel>
      </div>
      <div className="Index-MottoSection">
        <Center>
          <div className="Index-Motto-Image">
            <img src={quotationMark} alt="Quotation Mark" />
          </div>
          <div className="Index-Animation-Motto">
            <Ghost
              timer={5000}
              timeC={3}
              active={0}
              style={{ fontSize: "2.7rem" }}
            >
              想像的力量，創造無限可能
            </Ghost>
            <Ghost timer={5000} timeC={3} active={1}>
              Power of imagination make us infinite
            </Ghost>
            <Ghost timer={5000} timeC={3} active={2}>
              Ideas make everything happen
            </Ghost>
          </div>
        </Center>
      </div>
      <div className="Index-PosterSection">
        <div style={{ paddingTop: "12%" }}>
          <a href="/icolorchart">
            <img src={poster} alt="" />
          </a>
        </div>
      </div>
      <div className="Index-FormTabSection">
        <Tabs>
          <Tab
            className={
              openTab === 0 ? "Index-FormTab-Product active" : "FormTab-Product"
            }
            onClick={() => {
              handleTabChange(0);
            }}
          >
            產品查詢
          </Tab>
          <Tab
            className={
              openTab === 1 ? "Index-FormTab-Coop active" : "FormTab-Coop"
            }
            onClick={() => {
              handleTabChange(1);
            }}
          >
            商務合作
          </Tab>
        </Tabs>
        <div
          className={
            openTab === 0
              ? "Index-FormTab-Container Product"
              : "Index-FormTab-Container Coop"
          }
        >
          <div className="Index-FormTab-FormContainer">{form}</div>
          <div className="Index-FormTab-DescriptionContainer">
            {formDescription}
          </div>
        </div>
      </div>
      <div className="Index-SNSection">
        <a
          href="https://www.facebook.com/IDAProfessional/?fref=ts"
          target="_blank"
        >
          <img src={fbIcon} alt=""></img>
        </a>
        <a href="https://www.youtube.com/watch?v=BFWrtaAwxYU" target="_blank">
          <img src={ytIcon} alt=""></img>
        </a>
        {/* <a href=""> */}
        <div onClick={() => handleModalOpen(1)}>
          <img src={wcIcon} alt=""></img>
        </div>
        {/* </a> */}
        <a href="https://www.instagram.com/ida_professional/" target="_blank">
          <img src={igIcon} alt=""></img>
        </a>
        <a href="http://shopping.ida.com/" target="_blank">
          <img src={buyIcon} alt=""></img>
        </a>
        <div className="Index-UpTopContainer">
          <a href="#frontpageTop">
            <img className="Index-UpTop" src={upTop} alt="Up to Top" />
          </a>
        </div>
      </div>
      {/* <div className="Index-UpTopSection">
        <div className="Index-UpTopContainer">
          <a href="#frontpageTop">
            <img className="Index-UpTop" src={upTop} alt="Up to Top" />
          </a>
        </div>
      </div> */}
      <Footer />
    </div>
  );
};

export default HomeScreen;
