import React from "react";
import "./IColorChartScreen.css";

import home from "../../assets/img/icolorchart/home.svg";
import back from "../../assets/img/icolorchart/back1.jpg";
import corner from "../../assets/img/icolorchart/2_23-30.svg";
import title from "../../assets/img/icolorchart/2_2-22.svg";
import hk from "../../assets/img/icolorchart/apple/hk.svg";
import cn from "../../assets/img/icolorchart/apple/cn.svg";

import Slider from "react-slick";

import s1 from "../../assets/img/icolorchart/1.jpg";
import s2 from "../../assets/img/icolorchart/2.jpg";
import s3 from "../../assets/img/icolorchart/3.jpg";
import s4 from "../../assets/img/icolorchart/4.jpg";
import s5 from "../../assets/img/icolorchart/5.jpg";
import s6 from "../../assets/img/icolorchart/6.jpg";
import s7 from "../../assets/img/icolorchart/7.jpg";

const AppleScreen = () => {
  const settings = {
    arrows: false,
    autoplay: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className="ICC-Android-Main">
      <img src={back} alt="" className="ICC-Android-Back" />
      <img src={corner} alt="" className="ICC-Android-Corner" />
      <div className="ICC-Android-Home">
        <a href="/">
          <img src={home} alt="" />
        </a>
      </div>
      <div className="ICC-Android-Content">
        <img src={title} alt="" className="ICC-Android-Title" />
        {/* <img src={phone} alt="" className="ICC-Android-Phone" /> */}
        <div className="ICC-Android-Slider">
          <Slider {...settings}>
            <div>
              <img src={s1} alt="" />
            </div>
            <div>
              <img src={s2} alt="" />
            </div>
            <div>
              <img src={s3} alt="" />
            </div>
            <div>
              <img src={s4} alt="" />
            </div>
            <div>
              <img src={s5} alt="" />
            </div>
            <div>
              <img src={s6} alt="" />
            </div>
            <div>
              <img src={s7} alt="" />
            </div>
          </Slider>
        </div>
        <div className="ICC-Apple-Button-Container">
          <a
            href="https://www.halcos.cn/apps/appstore/install_count.php?app=iColorChart&os=android&app_region=hk&app_device=mobile"
            className="ICC-Apple-Hk-Button"
          >
            <img src={hk} alt="" className="ICC-Apple-Hk" />
          </a>
          <a
            href="https://www.halcos.cn/apps/appstore/install_count.php?app=iColorChart&os=ios&app_region=cn&app_device=mobile"
            className="ICC-Apple-CN-Button"
          >
            <img src={cn} alt="" className="ICC-Apple-Cn" />
          </a>
        </div>
        <div className="ICC-Apple-Caution">
          <p>iPhone/iPad 用戶注意：</p>
          <p>
            App 必需在設定中選取信任 "World Hair Cosmetics (Asia)
            Limited"。按此了解詳細指示
          </p>
        </div>
      </div>
    </div>
  );
};

export default AppleScreen;
