import React from "react";
import "./IColorChartScreen.css";

import back from "../../assets/img/icolorchart/back1.jpg";
import corner from "../../assets/img/icolorchart/2_23-30.svg";
import title from "../../assets/img/icolorchart/2_2-22.svg";
import phone from "../../assets/img/icolorchart/2_23-29.png";
import android from "../../assets/img/icolorchart/2_22-23.svg";
import ios from "../../assets/img/icolorchart/2_22-26.svg";
import home from "../../assets/img/icolorchart/home.svg";

const IColorChartScreen = () => {
  return (
    <div style={{ position: "relative" }}>
      <div className="ICC-HomeContainer">
        <a href="/">
          <img src={home} alt="" className="ICC-Home" />
        </a>
      </div>
      <div className="ICC-Main">
        <img src={back} alt="" className="ICC-Back" />
        <img src={corner} alt="" className="ICC-Corner" />
        <div>
          <img src={title} alt="" className="ICC-Title" />
          <img src={phone} alt="" className="ICC-Phone" />
          <a href="/android" className="ICC-Android-Button">
            <img src={android} alt="" className="ICC-Android" />
          </a>
          <a href="/apple" className="ICC-Ios-Button">
            <img src={ios} alt="" className="ICC-Ios" />
          </a>
        </div>
        {/* <img src={} alt="" /> */}
      </div>
    </div>
  );
};

export default IColorChartScreen;
