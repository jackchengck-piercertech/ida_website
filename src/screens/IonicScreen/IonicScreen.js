import React, { useEffect } from "react";
import "./IonicScreen.css";
import AnchorLink from "react-anchor-link-smooth-scroll";
import FV from "../../components/FV/FV";

import logo from "../../assets/img/ionic/ionic3.jpg";
import bctext from "../../assets/img/ionic/ionic text 3.png";
import betext from "../../assets/img/ionic/svg-297204x59.svg";
import bgirl from "../../assets/img/ionic/girl4.png";

import cTitle from "../../assets/img/ionic/svg-347016x104.svg";
import cp1 from "../../assets/img/ionic/c685 l0.png";
import cp2 from "../../assets/img/ionic/c685 l1.png";
import cp3 from "../../assets/img/ionic/c685 l2.png";
import cp4 from "../../assets/img/ionic/c685 g0.png";

import cp1i from "../../assets/img/ionic/svg-24059x96.svg";
import cp2i from "../../assets/img/ionic/svg-237257x96.svg";
import cp3i from "../../assets/img/ionic/svg-24059x962.svg";
import cp4i1 from "../../assets/img/ionic/g0 text.png";
import cp4i2 from "../../assets/img/ionic/svg-237257x37.svg";

import spacer1 from "../../assets/img/ionic/ionic line02.png";
import spacer2 from "../../assets/img/ionic/ionic line03.png";

import sTitle from "../../assets/img/ionic/svg-266895x104.svg";
import sp1 from "../../assets/img/ionic/g3.png";
import sp2 from "../../assets/img/ionic/g5.png";
import sp3 from "../../assets/img/ionic/g9.png";
import sp4 from "../../assets/img/ionic/n.png";

import sp1i from "../../assets/img/ionic/svg-198218x96.svg";
import sp2i from "../../assets/img/ionic/svg-270218x96.svg";
import sp3i from "../../assets/img/ionic/svg-198218x962.svg";
import sp4i from "../../assets/img/ionic/svg-204892x96.svg";

import rTitle from "../../assets/img/ionic/svg-294109x126.svg";
import rp1 from "../../assets/img/ionic/c332.png";
import rp21 from "../../assets/img/ionic/c331.png";
import rp22 from "../../assets/img/ionic/c333.png";
import rp3 from "../../assets/img/ionic/c481.png";
import rp4 from "../../assets/img/ionic/c482.png";

import rp1i from "../../assets/img/ionic/svg-218103x96.svg";
import rp2i1 from "../../assets/img/ionic/svg-210487x96.svg";
import rp2i2 from "../../assets/img/ionic/svg-336733x96.svg";
import rp3i from "../../assets/img/ionic/svg-304917x115.svg";
import rp4i from "../../assets/img/ionic/svg-248623x96.svg";

import topButton from "../../assets/img/nallo-color/svg 39x39_poster_.png";
import up from "../../assets/img/ionic/up.png";
import Footer from "../../components/Footer/Footer";

const IonicScreen = (props) => {
  useEffect(() => {
    document.title = "Ionic";
  });

  return (
    <div className="Ionic-Main">
      <div className="Ionic-Banner" id="ionicBanner">
        <div className="Ionic-Banner-Container">
          <img
            src={logo}
            alt=""
            style={{ width: "95px", height: "95px", paddingBottom: "50px" }}
          />
          <img
            src={bctext}
            alt=""
            style={{ height: "55px", paddingBottom: "20px" }}
          />
          <img src={betext} alt="" style={{ height: "70px" }} />
        </div>
        <div className="Ionic-Banner-G">
          <img src={bgirl} alt="" />
        </div>
      </div>
      <div className="Ionic-Curly">
        <div className="Ionic-Curly-Title">
          <img src={cTitle} alt="" />
        </div>
        <div className="Ionic-Curly-Row">
          <div>
            <div>
              <img src={cp1} alt="" />
            </div>
            <div>
              <img src={cp1i} alt="" />
            </div>
          </div>
          <div>
            <div>
              <img src={cp2} alt="" />
            </div>
            <div>
              <img src={cp2i} alt="" />
            </div>
          </div>
        </div>
        <div className="Ionic-Curly-Row">
          <div>
            <div>
              <img src={cp3} alt="" />
            </div>
            <div>
              <img src={cp3i} alt="" />
            </div>
          </div>
          <div>
            <div>
              <img src={cp4} alt="" />
            </div>
            <div>
              <img src={cp4i1} alt="" />
              <img src={cp4i2} alt="" />
            </div>
          </div>
        </div>
      </div>
      <div className="Ionic-Spacer">
        <img src={spacer1} alt="" />
      </div>
      <div className="Ionic-Straight">
        <div className="Ionic-Straight-Title">
          <img src={sTitle} alt="" />
        </div>
        <div className="Ionic-Straight-Row">
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Straight-PBox">
                  <img src={sp1} alt="" />
                </div>
              </FV>
            </div>
            <div>
              <FV pT={"30%"}>
                <div className="Ionic-Straight-IBox">
                  <img src={sp1i} alt="" />
                </div>
              </FV>
            </div>
          </div>
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Straight-PBox">
                  <img src={sp2} alt="" />
                </div>
              </FV>
            </div>
            <div>
              <FV pT={"30%"}>
                <div className="Ionic-Straight-IBox">
                  <img src={sp2i} alt="" />
                </div>
              </FV>
            </div>
          </div>
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Straight-PBox">
                  <img src={sp3} alt="" />
                </div>
              </FV>
            </div>
            <div>
              <FV pT={"30%"}>
                <div className="Ionic-Straight-IBox">
                  <img src={sp3i} alt="" />
                </div>
              </FV>
            </div>
          </div>
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Straight-PBox">
                  <img src={sp4} alt="" />
                </div>
              </FV>
            </div>
            <div>
              <FV pT={"30%"}>
                <div className="Ionic-Straight-IBox">
                  <img src={sp4i} alt="" />
                </div>
              </FV>
            </div>
          </div>
        </div>
      </div>
      <div className="Ionic-Spacer">
        <img src={spacer2} alt="" />
      </div>
      <div className="Ionic-Reinforce Ionic-Curly">
        <div className="Ionic-Reinforce-Title Ionic-Curly-Title">
          <img src={rTitle} alt="" />
        </div>
        <div className="Ionic-Reinforce-Row Ionic-Curly-Row">
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Reinforce-PBox">
                  <img src={rp1} alt="" />
                </div>
              </FV>
            </div>
            <div>
              <img src={rp1i} alt="" />
            </div>
          </div>
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Reinforce-PBox Twin">
                  <img src={rp21} alt="" />
                  <img
                    src={rp22}
                    alt=""
                    style={{
                      marginLeft: "-40px",
                    }}
                  />
                </div>
              </FV>
            </div>
            <div className="Ionic-Curly-PBoxContainer">
              <FV pT={"100%"}>
                <div className="Ionic-Reinforce-IBox Twin">
                  <div>
                    <img src={rp2i1} alt="" />
                  </div>
                  <div>
                    <img src={rp2i2} alt="" />
                  </div>
                </div>
              </FV>
            </div>
          </div>
        </div>
        <div className="Ionic-Reinforce-Row Ionic-Curly-Row">
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Reinforce-PBox">
                  <img src={rp3} alt="" />
                </div>
              </FV>
            </div>
            <div>
              <img src={rp3i} alt="" />
            </div>
          </div>
          <div>
            <div>
              <FV pT={"100%"}>
                <div className="Ionic-Reinforce-PBox">
                  <img
                    src={rp4}
                    alt=""
                    style={{ paddingTop: "30px", paddingRight: "80px" }}
                  />
                </div>
              </FV>
            </div>
            <div>
              <img src={rp4i} alt="" />
            </div>
          </div>
        </div>
      </div>
      <div className="Ionic-TopButton">
        <AnchorLink href="#ionicBanner">
          <img src={up} alt="" />
        </AnchorLink>
      </div>
      <Footer style={{ fontSize: "10px", marginBottom: "40px" }} />
    </div>
  );
};

export default IonicScreen;
