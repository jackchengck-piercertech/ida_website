import React, { Component } from "react";
import "./KttDetailScreen.css";
import Slider from "react-slick";

import s1 from "../../assets/img/ktt-detail/1.jpg";
import s2 from "../../assets/img/ktt-detail/2.jpg";
import s3 from "../../assets/img/ktt-detail/3.jpg";
import s4 from "../../assets/img/ktt-detail/4.jpg";
import s5 from "../../assets/img/ktt-detail/5.jpg";
import s6 from "../../assets/img/ktt-detail/6.jpg";
import s7 from "../../assets/img/ktt-detail/7.jpg";
import s8 from "../../assets/img/ktt-detail/8.jpg";
import s9 from "../../assets/img/ktt-detail/9.jpg";
import s10 from "../../assets/img/ktt-detail/10.jpg";
import s11 from "../../assets/img/ktt-detail/11.jpg";
import s12 from "../../assets/img/ktt-detail/12.jpg";
import s13 from "../../assets/img/ktt-detail/13.jpg";
import s14 from "../../assets/img/ktt-detail/14.jpg";

export default class KttDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  render() {
    const setting = {
      autoplay: true,
      infinite: true,
      slidesToShow: 1,
      autoplaySpeed: 2000,
      dots: false,
      arrows: false,
    };

    return (
      <div className="KD-Main">
        <div className="KD-Arrows-Container">
          <div className="KD-Arrows" onClick={this.next}>
            <p>{"<"}</p>
          </div>
          <div className="KD-Arrows" onClick={this.previous}>
            <p>{">"}</p>
          </div>
        </div>
        <Slider
          ref={(c) => (this.slider = c)}
          {...setting}
          // style={{ height: "100vh", width: "100vw" }}
        >
          <div>
            <img src={s1} alt="" />
          </div>
          <div>
            <img src={s2} alt="" />
          </div>
          <div>
            <img src={s3} alt="" />
          </div>
          <div>
            <img src={s4} alt="" />
          </div>
          <div>
            <img src={s5} alt="" />
          </div>
          <div>
            <img src={s6} alt="" />
          </div>
          <div>
            <img src={s7} alt="" />
          </div>
          <div>
            <img src={s8} alt="" />
          </div>
          <div>
            <img src={s9} alt="" />
          </div>
          <div>
            <img src={s10} alt="" />
          </div>
          <div>
            <img src={s11} alt="" />
          </div>
          <div>
            <img src={s12} alt="" />
          </div>
          <div>
            <img src={s13} alt="" />
          </div>
          <div>
            <img src={s14} alt="" />
          </div>
        </Slider>
      </div>
    );
  }
}
