import React, { useState, useEffect } from "react";
import "./NalloColorScreen.css";

import Center from "../../components/Center/Center";
import FV from "../../components/FV/FV";
import AnchorLink from "react-anchor-link-smooth-scroll";
import { Modal, Fade } from "@material-ui/core";

import bannerL from "../../assets/img/nallo-color/032.png";
import bannerR from "../../assets/img/nallo-color/png01.png";
import tube from "../../assets/img/nallo-color/nallo 3 tube.png";
import info from "../../assets/img/nallo-color/svg-473884x234.svg";
import mobileInfo from "../../assets/img/nallo-color/mobile-info.svg";
import DropletButton from "../../components/DropletButton/DropletButton";
import CIButton from "../../assets/img/nallo-color/svg-283125x444.svg";
import CSButton from "../../assets/img/nallo-color/svg-25629x432.svg";
import g1 from "../../assets/img/nallo-color/u24511-11.png";
import g1Button from "../../assets/img/nallo-color/svg 55336x9_poster_.png";
import g2 from "../../assets/img/nallo-color/nallo chart photo.jpg";
import g3 from "../../assets/img/nallo-color/nallo tube.png";
import g4 from "../../assets/img/nallo-color/nallo shampoo front.jpg";
import g5 from "../../assets/img/nallo-color/nallo lite.png";
import g6 from "../../assets/img/nallo-color/nallo premium tube.png";
import topButton from "../../assets/img/nallo-color/svg 39x39_poster_.png";
import mc1 from "../../assets/img/nallo-color/svg-372931x217.svg";
import mc2 from "../../assets/img/nallo-color/svg 381299x259.svg";
import mc3 from "../../assets/img/nallo-color/svg 35602x258.svg";
import bc from "../../assets/img/nallo-color/svg 273603x199.svg";
import Footer from "../../components/Footer/Footer";

const NalloColorScreen = (props) => {
  const [modalContent, setModalContent] = useState(0);
  const [open, setOpen] = useState(false);
  useEffect(() => {
    document.title = "Nallo Color";
  });
  const handleModalOpen = (contentNum) => {
    setModalContent(contentNum);
    setOpen(true);
  };

  const handleModalClose = () => {
    setOpen(false);
  };

  let modalBody = (
    <div className="Nallo-Modal">
      <div>This is Modal Body</div>
    </div>
  );

  switch (modalContent) {
    case 0:
      modalBody = (
        <div className="Nallo-Modal">
          <div>
            <img
              src={mc1}
              alt=""
              style={{ maxWidth: "477px", width: "100%" }}
            />
            <img src={g3} alt="" />
          </div>
        </div>
      );
      break;
    case 1:
      modalBody = (
        <div className="Nallo-Modal">
          <div>
            <img
              src={mc2}
              alt=""
              style={{ maxWidth: "477px", width: "100%" }}
            />
            <img src={g5} alt="" />
          </div>
        </div>
      );
      break;
    case 2:
      modalBody = (
        <div className="Nallo-Modal">
          <div>
            <img
              src={mc3}
              alt=""
              style={{ maxWidth: "455px", width: "100%" }}
            />
            <img src={g6} alt="" />
          </div>
        </div>
      );
      break;
    default:
      modalBody = (
        <div className="Nallo-Modal">
          <div>
            <img
              src={mc1}
              alt=""
              style={{ maxWidth: "477px", width: "100%" }}
            />
            <img src={g3} alt="" />
          </div>
        </div>
      );
  }

  return (
    <div>
      <Modal open={open} onClose={handleModalClose}>
        <Fade in={open} timeout={800}>
          {modalBody}
        </Fade>
      </Modal>
      <div className="Nallo-Banner" id="nalloBanner">
        <div className="Nallo-Banner-Container">
          <img src={bannerL} alt="" style={{ width: "35%" }} />
          <img src={bannerR} alt="" style={{ width: "65%" }} />
        </div>
      </div>
      <div className="Nallo-Info Main">
        <div className="Nallo-Info-L">
          <img src={tube} alt="" />
        </div>
        <div className="Nallo-Info-R">
          <FV pT={"90%"}>
            <Center>
              <div>
                <img src={info} alt="" style={{ width: "100%" }} />
              </div>
              <div className="Nallo-Info-Button">
                <DropletButton>
                  <a href="/coloring-issue">
                    <img src={CIButton} alt="" />
                  </a>
                </DropletButton>
                <DropletButton>
                  <a href="/color-system">
                    <img src={CSButton} alt="" />
                  </a>
                </DropletButton>
              </div>
            </Center>
          </FV>
        </div>
      </div>
      <div className="Nallo-Info Mobile">
        <div className="Nallo-Info-MobileContainer">
          <div className="Nallo-Info-L">
            <img src={tube} alt="" />
          </div>
          <div className="Nallo-Info-R">
            <Center>
              <div>
                <img src={mobileInfo} alt="" style={{ width: "100%" }} />
              </div>
            </Center>
          </div>
        </div>
        <div className="Nallo-Info-Button">
          <DropletButton>
            <a href="/coloring-issue">
              <img src={CIButton} alt="" />
            </a>
          </DropletButton>
          <DropletButton>
            <a href="/color-system">
              <img src={CSButton} alt="" />
            </a>
          </DropletButton>
        </div>
      </div>
      <div className="Nallo-Grid">
        <FV pT={"50%"}>
          <div className="Nallo-Grid-Container">
            <div className="Nallo-Grid-1">
              <img
                src={g1}
                alt="嶄新技術，矚目登場。 全面提升色彩潤澤度、髮質柔順度，給你不一樣的清新染髮新體驗。 Thoroughly improve hair vibrancy, smoothness and softness. Offers you a brand new hair coloring experience."
              />
              <div className="Nallo-Grid-1-Button">
                <div>
                  <DropletButton className="greenBg">
                    <a href="/nallo-color-chart">
                      <img src={g1Button} alt="Color Chart" />
                    </a>
                  </DropletButton>
                </div>
              </div>
            </div>
            <div className="Nallo-Grid-2">
              <div className="Nallo-Grid-2-1">
                <img src={g2} alt="" />
              </div>
              <div
                className="Nallo-Grid-2-2"
                onClick={() => handleModalOpen(0)}
              >
                <div>
                  <img src={g3} alt="" />
                </div>
              </div>
              <div className="Nallo-Grid-2-3">
                <div className="Nallo-Grid-2-3-back">
                  <div className="Nallo-Grid-2-3-flip">
                    <img src={g4} alt="" />
                  </div>
                  <img src={bc} alt="" />
                </div>
              </div>
              <div
                className="Nallo-Grid-2-4"
                onClick={() => handleModalOpen(1)}
              >
                <div>
                  <img src={g5} alt="" />
                </div>
              </div>
              <div
                className="Nallo-Grid-2-5"
                onClick={() => handleModalOpen(2)}
              >
                <div>
                  <img src={g6} alt="" />
                </div>
              </div>
            </div>
          </div>
        </FV>
      </div>
      <div className="Nallo-TopButton">
        <AnchorLink href="#nalloBanner">
          <img src={topButton} alt="" />
        </AnchorLink>
      </div>
      <Footer style={{ fontSize: "10px", marginBottom: "20px" }} />
    </div>
  );
};

export default NalloColorScreen;
