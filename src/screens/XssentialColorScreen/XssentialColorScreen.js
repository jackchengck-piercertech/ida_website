import React, { useEffect } from "react";
import "./XssentialColorScreen.css";

import Footer from "../../components/Footer/Footer";

import bannerL from "../../assets/img/xssential-color/xssential banner.png";
import bannerR from "../../assets/img/xssential-color/xssential banner text.png";
import tubeL from "../../assets/img/xssential-color/xssential tube.png";
import tubeR from "../../assets/img/xssential-color/xssential tube(manicure).png";
import infoL from "../../assets/img/xssential-color/svg-318618x167.svg";
import infoR from "../../assets/img/xssential-color/svg-479284x185.svg";
import infoRm from "../../assets/img/xssential-color/manicure-info-m.svg";
import DropletButton from "../../components/DropletButton/DropletButton";
import ccB from "../../assets/img/xssential-color/svg-55336x9.svg";
import ciB from "../../assets/img/xssential-color/svg-283125x445.svg";
import csB from "../../assets/img/xssential-color/svg-25629x433.svg";
import hc0 from "../../assets/img/xssential-color/hair-colors/01.png";
import hc1 from "../../assets/img/xssential-color/hair-colors/19.png";
import hc2 from "../../assets/img/xssential-color/hair-colors/14.png";
import hc3 from "../../assets/img/xssential-color/hair-colors/18.png";
import hc4 from "../../assets/img/xssential-color/hair-colors/17.png";
import hc5 from "../../assets/img/xssential-color/hair-colors/16.png";
import hc6 from "../../assets/img/xssential-color/hair-colors/05.png";
import hc7 from "../../assets/img/xssential-color/hair-colors/08.png";
import hc8 from "../../assets/img/xssential-color/hair-colors/10.png";
import hc9 from "../../assets/img/xssential-color/hair-colors/12.png";

const XssentialColorScreen = (props) => {
  useEffect(() => {
    document.title = "Xssential Color";
  });
  return (
    <div className="Xssentail-Main">
      <div className="Xssential-Banner">
        <div className="Xssential-Banner-Container">
          <img src={bannerL} alt="" className="Xssential-Banner-Image-L" />
          <img src={bannerR} alt="" className="Xssential-Banner-Image-R" />
        </div>
      </div>
      <div className="Xssential-Info">
        <div className="Xssential-Info-L">
          <img src={tubeL} alt="" />
          <div>
            <img src={infoL} alt="" />
            <div className="Xssential-Info-L-Button">
              <div>
                <DropletButton className="purpleBg">
                  <a href="/xssential-color-chart">
                    <img src={ccB} alt="" />
                  </a>
                </DropletButton>
              </div>
              <DropletButton className="purple">
                <a href="/coloring-issue">
                  <img src={ciB} alt="" />
                </a>
              </DropletButton>
              <DropletButton className="purple">
                <a href="/color-system">
                  <img src={csB} alt="" />
                </a>
              </DropletButton>
            </div>
          </div>
        </div>
        <div className="Xssential-Info-R">
          <img src={tubeR} alt="" />
          <div>
            <img src={infoR} alt="" className="Main" />
            <img src={infoRm} alt="" className="Mobile" />
            <div className="Xssential-Info-R-HairColor">
              <div>
                <img src={hc0} alt="" />
                <img src={hc1} alt="" />
                <img src={hc2} alt="" />
                <img src={hc3} alt="" />
                <img src={hc4} alt="" />
              </div>
              <div>
                <img src={hc5} alt="" />
                <img src={hc6} alt="" />
                <img src={hc7} alt="" />
                <img src={hc8} alt="" />
                <img src={hc9} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer style={{ fontSize: "10px", marginBottom: "20px" }} />
    </div>
  );
};

export default XssentialColorScreen;
